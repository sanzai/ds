<?php
App::uses('AppModel', 'Model');
/**
 * PackingDetail Model
 *
 */
class PackingDetail extends AppModel {

     public $belongsTo = array(
        'Packing'=>array(
            'className' => 'packings',
            'foreignKey'=> '',
            'type'=>'left',
            'conditions'=> array('PackingDetail.packing_id = Packing.id'),
            'fields' => null ,
            'order'=> null ,
         ),
        'IncomingDetail'=>array(
            'className' => 'incoming_details',
            'foreignKey'=> '',
            'type'=>'left',
            'conditions'=> array('PackingDetail.incoming_details_id = IncomingDetail.id'),
            'fields' => null ,
            'order'=> null ,
         ),
        'MtSCd'=>array(
            'className' => 'mt_s_cds',
            'foreignKey'=> '',
            'type'=>'left',
            'conditions'=> array('IncomingDetail.s_cd = MtSCd.id'),
            'fields' => null ,
            'order'=> null ,
         ),


     );


}
