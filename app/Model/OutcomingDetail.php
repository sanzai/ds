<?php
App::uses('AppModel', 'Model');
/**
 * OutcomingDetail Model
 *
 */
class OutcomingDetail extends AppModel {

    public $hasOne = array(
        'mt_s_cds'=>array(
            'className' => 'mt_s_cds',
            'foreignKey'=> false,
            'type'=>'left',
            'conditions'=> array('OutcomingDetail.s_cd = mt_s_cds.ks_cd'),
            'fields' => null ,
            'order'=> null ,
            'dependent'=> true,
            'counterCache' => true ,
            'counterScope' => null
         ),
     );


}
