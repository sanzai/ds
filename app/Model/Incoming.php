<?php
App::uses('AppModel', 'Model');
/**
 * Incoming Model
 *
 */
class Incoming extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'incharge' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => '担当者を選択してください。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'incoming_plan' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => '入庫予定日を選択してください。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'containar_no' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'コンテナNoを選択してください。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'containar_div' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'コンテナ区分を選択してください。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'shipper' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => '荷送人を選択してください。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		);
}
