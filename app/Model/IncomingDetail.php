<?php
App::uses('AppModel', 'Model');
/**
 * IncomingDetail Model
 *
 */
class IncomingDetail extends AppModel {

     public $belongsTo = array(
        'MtSCd'=>array(
            'className' => 'mt_s_cds',
            'foreignKey'=> '',
            'type'=>'left',
            'conditions'=> array('IncomingDetail.s_cd = MtSCd.id'),
            'fields' => null ,
            'order'=> null ,
         ),
        'User'=>array(
            'className' => 'users',
            'foreignKey'=> '',
            'type'=>'left',
            'conditions'=> array('MtSCd.user_id = User.id'),
            'fields' => null ,
            'order'=> null ,
         ),


     );

}
