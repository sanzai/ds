<?php
App::uses('AppModel', 'Model');
/**
 * MtArea Model
 *
 */
class MtShipper extends AppModel {


/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'item_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => '値を入力してください。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}