<?php
App::uses('AppModel', 'Model');
/**
 * MtArea Model
 *
 */
class MtSCd extends AppModel {


	public $hasOne = array(
        'User'=>array(
            'className' => 'users',
            'foreignKey'=> 'id',
            'type'=>'left',
            //'conditions'=> array('IncomingDetail.s_cd = MtSCd.id'),
            'fields' => null ,
            'order'=> null ,
            'dependent'=> true,
            'counterCache' => true ,
            'counterScope' => null
         ),
     );


/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'item_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => '値を入力してください。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
