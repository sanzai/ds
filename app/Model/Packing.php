
<?php
App::uses('AppModel', 'Model');
/**
 * Packing Model
 *
 */
class Packing extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'packing_date' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => '加工日を選択してください。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'member' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => '人数を選択してください。',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}


