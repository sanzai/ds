<?php
App::uses('AppController', 'Controller');
/**
 * OutcomingDetails Controller
 *
 * @property OutcomingDetail $OutcomingDetail
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class OutcomingDetailsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	public $uses = array('OutcomingDetail','Incoming', 'IncomingDetail','Packing', 'PackingDetail','MtCarrier','MtUnit','MtArea','MtShipper','MtSize','MtSCd','Outcoming','setting');


/**
 * index method
 *
 * @return void
 */
	public function index($id) {

        //検索結果件数の設定
		$this->paginate = array(
			'limit' => 10,
        );

        //おまじない
        $this->OutcomingDetail->recursive = 0;

		//検索条件の指定
        $conditions = array('outcoming_id' => $id);
		//検索結果をセット
        $this->set('outcomingDetails', $this->Paginator->paginate($conditions));

		//$options = array('conditions' => array('outcoming_id' => $id));
		//$this->set('outcomingDetails', $this->OutcomingDetail->find('all', $options));

		$this->setListValue();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->OutcomingDetail->exists($id)) {
			throw new NotFoundException(__('Invalid outcoming detail'));
		}
		$options = array('conditions' => array('OutcomingDetail.' . $this->OutcomingDetail->primaryKey => $id));
		$this->set('outcomingDetail', $this->OutcomingDetail->find('first', $options));

		$this->setListValue();
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->OutcomingDetail->create();
			if ($this->OutcomingDetail->save($this->request->data)) {
				$this->Session->setFlash(__('The outcoming detail has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The outcoming detail could not be saved. Please, try again.'));
			}
		}
		$this->setListValue();
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->OutcomingDetail->exists($id)) {
			throw new NotFoundException(__('Invalid outcoming detail'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->OutcomingDetail->save($this->request->data)) {
				$this->Session->setFlash(__('The outcoming detail has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The outcoming detail could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('OutcomingDetail.' . $this->OutcomingDetail->primaryKey => $id));
			$this->request->data = $this->OutcomingDetail->find('first', $options);
		}
		$this->setListValue();
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->OutcomingDetail->id = $id;
		if (!$this->OutcomingDetail->exists()) {
			throw new NotFoundException(__('Invalid outcoming detail'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->OutcomingDetail->delete()) {
			$this->Session->setFlash(__('The outcoming detail has been deleted.'));
		} else {
			$this->Session->setFlash(__('The outcoming detail could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	function setListValue(){

		//コンテナ区分
		$containarOpt = array( '1'=>'ドライ','2'=>'リーファー');
		//荷送人
		$shipperOpt = $this->MtShipper->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//運送会社
		$carrierOpt = $this->MtCarrier->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//担当者
		$inchargeOpt = array( '1'=>'与那覇','2'=>'小宮');
		//商品名
		$sCdOpt = $this->MtSCd->find('list',array('fields'=>array('item_name'),'conditions'=>array('deleted is null')));
		//産地
		$areaOpt = $this->MtArea->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//規格
		$sizeOpt = $this->MtSize->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//単位
		$unitOpt = $this->MtUnit->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//$userOpt = $this->User->find('list',array('fields'=>array('user_name'),'conditions'=>array('deleted_at is null')));

		$this->set(compact('containarOpt','shipperOpt','carrierOpt','inchargeOpt','sCdOpt','areaOpt','sizeOpt','unitOpt'));

	}


}

