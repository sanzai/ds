<?php
App::uses('AppController', 'Controller');
/**
 * MtCarriers Controller
 *
 * @property MtCarrier $MtCarrier
 * @property PaginatorComponent $Paginator
 */
class MtCarriersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MtCarrier->recursive = 0;
		$this->set('mtCarriers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MtCarrier->exists($id)) {
			throw new NotFoundException(__('Invalid mt carrier'));
		}
		$options = array('conditions' => array('MtCarrier.' . $this->MtCarrier->primaryKey => $id));
		$this->set('mtCarrier', $this->MtCarrier->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MtCarrier->create();
			if ($this->MtCarrier->save($this->request->data)) {
				$this->Session->setFlash(__('The mt carrier has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mt carrier could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MtCarrier->exists($id)) {
			throw new NotFoundException(__('Invalid mt carrier'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MtCarrier->save($this->request->data)) {
				$this->Session->setFlash(__('The mt carrier has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mt carrier could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MtCarrier.' . $this->MtCarrier->primaryKey => $id));
			$this->request->data = $this->MtCarrier->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MtCarrier->id = $id;
		if (!$this->MtCarrier->exists()) {
			throw new NotFoundException(__('Invalid mt carrier'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MtCarrier->delete()) {
			$this->Session->setFlash(__('The mt carrier has been deleted.'));
		} else {
			$this->Session->setFlash(__('The mt carrier could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
