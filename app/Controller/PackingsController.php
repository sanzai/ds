<?php
App::uses('AppController', 'Controller');
/**
 * Packings Controller
 *
 * @property Packing $Packing
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PackingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	public $uses = array('Packing','PackingDetail','MtCarrier','MtUnit','MtArea','MtShipper','MtSize','MtSCd');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Packing->recursive = 0;
		$this->set('packings', $this->Paginator->paginate());

		$this->setListValue();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Packing->exists($id)) {
			throw new NotFoundException(__('Invalid packing'));
		}

		//-------------------------------
		//パッキングの基本情報取得
		//-------------------------------
		//基本情報の検索条件設定
		$options = array('conditions' => array('Packing.' . $this->Packing->primaryKey => $id));
		//検索結果セット
		$this->set('packing', $this->Packing->find('first', $options));

		//-------------------------------
		//基本情報にひもづく詳細情報取得
		//-------------------------------
		//基本情報の検索条件設定
		$options = array('conditions' => array('packing_id'=>$id));
		//基本情報の検索条件設定
		$this->set('packingDetails', $this->PackingDetail->find('all', $options));

		//選択条件の値を設定
		$this->setListValue();
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		//入力画面の値が設定されてる場合
		if ($this->request->is('post')) {

			//ＤＢ登録用の入れ物を生成
			$this->Packing->create();

			//登録を実行し、正常に終了した場合
			if ($this->Packing->save($this->request->data)) {
				//画面に一時表示用の文言を設定
				$this->Session->setFlash(__('The packing has been saved.'));
				//検索一覧画面へリダイレクト
				return $this->redirect(array('action' => 'index'));
			}
			//登録を実行し、エラーの場合
			else {
				$this->Session->setFlash(__('The packing could not be saved. Please, try again.'));
			}
		}
		$this->setListValue();
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Packing->exists($id)) {
			throw new NotFoundException(__('Invalid packing'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Packing->save($this->request->data)) {
				$this->Session->setFlash(__('The packing has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The packing could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Packing.' . $this->Packing->primaryKey => $id));
			$this->request->data = $this->Packing->find('first', $options);
		}
		$this->setListValue();
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Packing->id = $id;
		if (!$this->Packing->exists()) {
			throw new NotFoundException(__('Invalid packing'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Packing->delete()) {
			$this->Session->setFlash(__('The packing has been deleted.'));
		} else {
			$this->Session->setFlash(__('The packing could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	function setListValue()
	{

		//コンテナ区分
		$containarOpt = array( '1'=>'ドライ','2'=>'リーファー');
		//荷送人
		$shipperOpt = $this->MtShipper->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//運送会社
		$carrierOpt = $this->MtCarrier->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//担当者
		$inchargeOpt = array( '1'=>'与那覇','2'=>'小宮');
		//商品名
		$sCdOpt = $this->MtSCd->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//産地
		$areaOpt = $this->MtArea->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//規格
		$sizeOpt = $this->MtSize->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//単位
		$unitOpt = $this->MtUnit->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));

		$this->set(compact('containarOpt','shipperOpt','carrierOpt','inchargeOpt','sCdOpt','areaOpt','sizeOpt','unitOpt'));

		}

}

