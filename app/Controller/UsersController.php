<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());

		$this->setListValue();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('登録完了です'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
		$this->setListValue();
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$this->setListValue();
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	function setListValue(){

		//コンテナ区分
		$containarOpt = array( '1'=>'ドライ','2'=>'リーファー');
		//荷送人
		$shipperOpt = array( '1'=>'福岡エニックスフーズ','2'=>'八女フードセンター','3'=>'鹿児島中央青果','4'=>'冨士平川青果');
		//運送会社
		$carrierOpt = array( '1'=>'毎日急行','2'=>'琉球通運');
		//担当者
		$inchargeOpt = array( '1'=>'与那覇','2'=>'小宮','3'=>'山川');
		//ユーザ権限
		$userOpt = array( '1'=>'ＤＳ','2'=>'金秀');
		//ユーザ区分
		$authOpt = array( '1'=>'管理者','2'=>'一般','3'=>'閲覧のみ');
		//商品名
		$useridOpt = $this->User->find('list',array('fields'=>array('user_name'),'conditions'=>array('user_div = 2 and deleted is null')));

		$this->set(compact('containarOpt','shipperOpt','carrierOpt','inchargeOpt','userOpt','authOpt','sCdOpt','packingflgOpt','useridOpt'));

	}

}


