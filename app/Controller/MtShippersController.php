<?php
App::uses('AppController', 'Controller');
/**
 * MtShippers Controller
 *
 * @property MtShipper $MtShipper
 * @property PaginatorComponent $Paginator
 */
class MtShippersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MtShipper->recursive = 0;
		$this->set('mtShippers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MtShipper->exists($id)) {
			throw new NotFoundException(__('Invalid mt shipper'));
		}
		$options = array('conditions' => array('MtShipper.' . $this->MtShipper->primaryKey => $id));
		$this->set('mtShipper', $this->MtShipper->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MtShipper->create();
			if ($this->MtShipper->save($this->request->data)) {
				$this->Session->setFlash(__('The mt shipper has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mt shipper could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MtShipper->exists($id)) {
			throw new NotFoundException(__('Invalid mt shipper'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MtShipper->save($this->request->data)) {
				$this->Session->setFlash(__('The mt shipper has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mt shipper could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MtShipper.' . $this->MtShipper->primaryKey => $id));
			$this->request->data = $this->MtShipper->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MtShipper->id = $id;
		if (!$this->MtShipper->exists()) {
			throw new NotFoundException(__('Invalid mt shipper'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MtShipper->delete()) {
			$this->Session->setFlash(__('The mt shipper has been deleted.'));
		} else {
			$this->Session->setFlash(__('The mt shipper could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
