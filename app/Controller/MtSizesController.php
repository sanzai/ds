<?php
App::uses('AppController', 'Controller');
/**
 * MtSizes Controller
 *
 * @property MtSize $MtSize
 * @property PaginatorComponent $Paginator
 */
class MtSizesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {

        $this->paginate = array(
                    'limit' => 10,
        );
		$this->MtSize->recursive = 0;
		$this->set('mtSizes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MtSize->exists($id)) {
			throw new NotFoundException(__('Invalid mt size'));
		}
		$options = array('conditions' => array('MtSize.' . $this->MtSize->primaryKey => $id));
		$this->set('mtSize', $this->MtSize->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MtSize->create();
			if ($this->MtSize->save($this->request->data)) {
				$this->Session->setFlash(__('The mt size has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mt size could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MtSize->exists($id)) {
			throw new NotFoundException(__('Invalid mt size'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MtSize->save($this->request->data)) {
				$this->Session->setFlash(__('The mt size has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mt size could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MtSize.' . $this->MtSize->primaryKey => $id));
			$this->request->data = $this->MtSize->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MtSize->id = $id;
		if (!$this->MtSize->exists()) {
			throw new NotFoundException(__('Invalid mt size'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MtSize->delete()) {
			$this->Session->setFlash(__('The mt size has been deleted.'));
		} else {
			$this->Session->setFlash(__('The mt size could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
