<?php
App::uses('AppController', 'Controller');
/**
 * Incomings Controller
 *
 * @property Incoming $Incoming
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IncomingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public $uses = array('Incoming', 'IncomingDetail','Packing', 'PackingDetail','MtCarrier','MtUnit','MtArea','MtShipper','MtSize','MtSCd','Outcoming','OutcomingDetail','setting','stock');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Incoming->recursive = 0;
		$this->set('incomings', $this->Paginator->paginate());

		$this->setListValue();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Incoming->exists($id)) {
			throw new NotFoundException(__('Invalid incoming'));
		}
		$options = array('conditions' => array('Incoming.' . $this->Incoming->primaryKey => $id));
		$this->set('incoming', $this->Incoming->find('first', $options));

		$options = array('conditions' => array('incoming_id'=>$id));
		$this->set('incomingDetails', $this->IncomingDetail->find('all', $options));


		$this->setListValue();
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Incoming->create();
			if ($this->Incoming->save($this->request->data)) {
				$this->Session->setFlash(__('The incoming has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The incoming could not be saved. Please, try again.'));
			}
		}

		$this->setListValue();
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Incoming->exists($id)) {
			throw new NotFoundException(__('Invalid incoming'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Incoming->save($this->request->data)) {
				$this->Session->setFlash(__('The incoming has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The incoming could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Incoming.' . $this->Incoming->primaryKey => $id));
			$this->request->data = $this->Incoming->find('first', $options);
		}

		$this->setListValue();
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Incoming->id = $id;
		if (!$this->Incoming->exists()) {
			throw new NotFoundException(__('Invalid incoming'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Incoming->delete()) {
			$this->Session->setFlash(__('The incoming has been deleted.'));
		} else {
			$this->Session->setFlash(__('The incoming could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	function setListValue(){

		//コンテナ区分
		$containarOpt = array( '1'=>'ドライ','2'=>'リーファー');
		//荷送人
		$shipperOpt = $this->MtShipper->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//運送会社
		$carrierOpt = $this->MtCarrier->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//担当者
		$inchargeOpt = array( '1'=>'与那覇','2'=>'小宮','3'=>'山川');
		//商品名
		$sCdOpt = $this->MtSCd->find('list',array('fields'=>array('item_name'),'conditions'=>array('deleted is null')));
		//産地
		$areaOpt = $this->MtArea->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//規格
		$sizeOpt = $this->MtSize->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//単位
		$unitOpt = $this->MtUnit->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//$userOpt = $this->User->find('list',array('fields'=>array('user_name'),'conditions'=>array('deleted_at is null')));

		$this->set(compact('containarOpt','shipperOpt','carrierOpt','inchargeOpt','sCdOpt','areaOpt','sizeOpt','unitOpt','useridOpt'));

	}



}




