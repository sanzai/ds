<?php
App::uses('AppController', 'Controller');
/**
 * Orders Controller
 *
 * @property Order $Order
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class StocksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	public $uses = array('IncomingDetail','Order','MtUnit','MtArea','MtSize','MtSCd','User','Incoming');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Order->recursive = 0;
		//$this->set('orders', $this->Paginator->paginate());

		//入庫した商品のうち、在庫が残っている商品を抽出

		//まず、入庫した商品を表示
		$this->IncomingDetail->recursive = 3;
		$options = array('conditions' => array());
		$this->set('stocks', $this->IncomingDetail->find('all', $options));

		//var_dump($this->IncomingDetail->find('all', $options));

		$this->setListValue();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Order->exists($id)) {
			throw new NotFoundException(__('Invalid order'));
		}
		$options = array('conditions' => array('Order.' . $this->Order->primaryKey => $id));
		$this->set('order', $this->Order->find('first', $options));

		$this->setListValue();
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Order->create();
			if ($this->Order->save($this->request->data)) {
				$this->Session->setFlash(__('The order has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The order could not be saved. Please, try again.'));
			}
		}
		$this->setListValue();
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Order->exists($id)) {
			throw new NotFoundException(__('Invalid order'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Order->save($this->request->data)) {
				$this->Session->setFlash(__('The order has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The order could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Order.' . $this->Order->primaryKey => $id));
			$this->request->data = $this->Order->find('first', $options);
		}
		$this->setListValue();
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Order->id = $id;
		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid order'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Order->delete()) {
			$this->Session->setFlash(__('The order has been deleted.'));
		} else {
			$this->Session->setFlash(__('The order could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}



    function upload(){

    	//$this->log($this->data,LOG_DEBUG);

        if (!empty($this->data)) {

            $file = $this->data['Order']['result']['tmp_name'];
            $reader = PHPExcel_IOFactory::createReader('Excel2007');
            $xl = $reader->load($file);
            $xl->setActiveSheetIndex();
            $sheet = $xl->getActiveSheet();

            $rowMax = $sheet->getHighestRow();
            $colMax = $sheet->getHighestColumn();
            $colsno = PHPExcel_Cell::columnIndexFromString($colMax);
            $data = array();

            $error = null;
            $save_result = 0;

            //for ($i = 2; $i <= 5; $i++) {
            for ($i = 2; $i <= $rowMax; $i++) {
                $colData = array();

                $this->Order->create();

                for ($j = 0; $j < 11; $j++) {
                    $colstr = PHPExcel_Cell::stringFromColumnIndex($j);
                    $cell = $sheet->getCell($colstr.$i);
                    $cellValue = $cell->getValue();
                    $colData[] = $cellValue;
                    //$this->log($colData[$j],LOG_DEBUG);
                }

                //$this->log($colData,LOG_DEBUG);
/*
                //名前を半角→全角→大文字へ変換
                $name = mb_convert_kana($colData[4],"R");
                $name = mb_strtoupper($name);

                //住所の数字部分を半角→全角へ変換
                $addr = mb_convert_kana($colData[7],"N");

                //既に登録されているユーザなら無視
				$options = array('conditions' => array('customer_name'=>$name));
				$result = $this->Customer->find('first', $options);
*/
				//var_dump($result);

				if(empty($result)){

					if(empty($colData[1])){
						continue;
					}
					// 登録する内容を設定
					$data = array('Order' =>
								array(
									'id' => null,
									'out_date' => $this->data['Order']['out_date'],
									's_cd' => $colData[0],
									'shop_cd' => $colData[3],
									'amount' => $colData[5],
									'created_at' => date('Y-m-d H:i:s'),
									'created_user_id' => 1
								)
							);

					// 登録する項目（フィールド指定）
					$fields = array('id', 'out_date', 's_cd', 'shop_cd', 'amount', 'created_at','created_user_id');

					if($this->Order->save($data, false, $fields)){
						$save_result++;
					}else{
						$this->Session->setFlash(__('履歴情報保存に失敗しました'));
						$error[] = array('line'=>$colData);
					}

				}

            }

            if(!empty($error)){
				$this->set('errors', $error);
            }

			$options = array('fields'=> array('out_date','s_cd', 'sum(amount) as cnt_amount'),'conditions' => array('out_date'=>$this->data['Order']['out_date']) ,'group' => array('s_cd'), );
			$this->set('orders', $this->Order->find('all', $options));

            $this->set('save_result', $save_result);

            $this->setListValue();

        }
    }

	function setListValue(){

		//商品名
		$sCdOpt = $this->MtSCd->find('list',array('fields'=>array('item_name'),'conditions'=>array('deleted is null')));
		//産地
		$areaOpt = $this->MtArea->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//規格
		$sizeOpt = $this->MtSize->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//単位
		$unitOpt = $this->MtUnit->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//バイヤー
		$buyerOpt = $this->User->find('list',array('fields'=>array('user_name'),'conditions'=>array('user_div = 2')));
		//ＤＳ担当
		$inchargeOpt = $this->User->find('list',array('fields'=>array('user_name'),'conditions'=>array('user_div = 1')));

		$this->set(compact('sCdOpt','areaOpt','sizeOpt','unitOpt','useridOpt','buyerOpt','inchargeOpt'));

	}


}
