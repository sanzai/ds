<?php
App::uses('AppController', 'Controller');
/**
 * MtUnits Controller
 *
 * @property MtUnit $MtUnit
 * @property PaginatorComponent $Paginator
 */
class MtUnitsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MtUnit->recursive = 0;
		$this->set('mtUnits', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MtUnit->exists($id)) {
			throw new NotFoundException(__('Invalid mt unit'));
		}
		$options = array('conditions' => array('MtUnit.' . $this->MtUnit->primaryKey => $id));
		$this->set('mtUnit', $this->MtUnit->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MtUnit->create();
			if ($this->MtUnit->save($this->request->data)) {
				$this->Session->setFlash(__('The mt unit has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mt unit could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MtUnit->exists($id)) {
			throw new NotFoundException(__('Invalid mt unit'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MtUnit->save($this->request->data)) {
				$this->Session->setFlash(__('The mt unit has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mt unit could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MtUnit.' . $this->MtUnit->primaryKey => $id));
			$this->request->data = $this->MtUnit->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MtUnit->id = $id;
		if (!$this->MtUnit->exists()) {
			throw new NotFoundException(__('Invalid mt unit'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MtUnit->delete()) {
			$this->Session->setFlash(__('The mt unit has been deleted.'));
		} else {
			$this->Session->setFlash(__('The mt unit could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
