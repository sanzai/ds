<?php
App::uses('AppController', 'Controller');
/**
 * PackingDetails Controller
 *
 * @property PackingDetail $PackingDetail
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PackingDetailsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	public $uses = array('PackingDetail', 'Packing','IncomingDetail', 'Incoming','MtCarrier','MtUnit','MtArea','MtShipper','MtSize','MtSCd','setting');


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->PackingDetail->recursive = 0;
		$this->set('packingDetails', $this->Paginator->paginate());

		$this->setListValue();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->PackingDetail->exists($id)) {
			throw new NotFoundException(__('不正なアクセスです。'));
		}
		$options = array('conditions' => array('PackingDetail.' . $this->PackingDetail->primaryKey => $id));
		$this->set('packingDetail', $this->IncomingDetail->find('first', $options));

		$this->setListValue();
	}

/**
 * viewbyscd method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function viewbyscd($incoming_details_id = null) {
		//if (!$this->PackingDetail->exists($incoming_details_id)) {
		//	throw new NotFoundException(__('不正なアクセスです。'));
		//}

		$this->PackingDetail->recursive = 2;
		$options = array('conditions' => array('PackingDetail.incoming_details_id' => $incoming_details_id));
		$this->set('packingDetails', $this->PackingDetail->find('all', $options));

		$this->IncomingDetail->recursive = 2;
		$options = array('conditions' => array('id' => $incoming_details_id));
		$this->set('incomingDetails', $this->IncomingDetail->find('first', $options));


		//var_dump($this->PackingDetail->find('all', $options));

		$this->setListValue();
	}


/**
 * add method
 *
 * @return void
 */
	public function add() {

		//$this->log($this->request,LOG_DEBUG);

		$incoming_detail_id = null;
		$packing_date = null;
		$packing_id = null;

		if( isset($this->request['data']['pack'])  ){

			$select_date_list = $this->request['data']['PackingDetail'];

			//選択された加工商品と加工日を取得
			while($select_date = current($select_date_list)){
				if($select_date != null){
					$incoming_detail_id = key($select_date_list);
					$packing_date = $select_date;
				}
				next($select_date_list);
			}

			//加工対象商品ＩＤを取得
			$incoming_detail_id = str_replace("select_date","",$incoming_detail_id);

			//選択された加工対象日が登録されているか？
			$options = array('conditions' => array('packing_date'=>$packing_date));
			$result = $this->Packing->find('first', $options);

			//$this->log(var_dump($result),LOG_DEBUG);


			if(empty($result)){
				$this->Packing->create();

				// 登録する内容を設定
				$data = array('Packing' =>
							array(
								'id' => null,
								'packing_date' => $packing_date,
								'created_at' => date('Y-m-d H:i:s'),
								'created_user_id' => 1
							)
						);

				// 登録する項目（フィールド指定）
				$fields = array('id', 'packing_date', 'created_at','created_user_id');

				if($this->Packing->save($data, false, $fields)){

					$options = array('conditions' => array('packing_date'=>$packing_date));
					$result = $this->Packing->find('first', $options);

					//$this->log("A",LOG_DEBUG);
					//$this->log(var_dump($result),LOG_DEBUG);

				}else{
					$this->Session->setFlash(__('履歴情報保存に失敗しました'));
					$error[] = array('line'=>$colData);
				}

			}else{

				$options = array('conditions' => array('packing_date'=>$packing_date));
				$result = $this->Packing->find('first', $options);

				$packing_id = $result['Packing']['id'];
				$packing_date = $result['Packing']['packing_date'];

				$this->log("B",LOG_DEBUG);
				$this->log($result['Packing']['id'],LOG_DEBUG);
				$this->log($result['Packing']['packing_date'],LOG_DEBUG);
			}
		}else{

			if ($this->request->is('post')) {

				$this->PackingDetail->create();
				if ($this->PackingDetail->save($this->request->data)) {
					$this->Session->setFlash(__('加工詳細情報の登録が完了しました。'));

					return $this->redirect(array('controller'=>'Stocks','action' => 'index'));

				} else {
					$this->Session->setFlash(__('加工詳細情報の登録に失敗しました。もう一度操作してください。'));
				}
			}
		}

		//パック対象商品情報を取得
		$this->IncomingDetail->recursive = 2;
		$options = array('conditions' => array('IncomingDetail.id'=>$incoming_detail_id));
		$this->set('incomingDetail', $this->IncomingDetail->find('first', $options));

		$this->set('incoming_detail_id', $incoming_detail_id);
		$this->set('packing_id', $packing_id);
		$this->set('packing_date', $packing_date);

		$this->setListValue();
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->PackingDetail->exists($id)) {
			throw new NotFoundException(__('不正なアクセスです。最初からやり直してください。'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->PackingDetail->save($this->request->data)) {

				//$this->log($this->request->data['IncomingDetail']['incoming_id'],LOG_DEBUG);

				$this->Session->setFlash(__('入庫詳細情報の更新が完了しました。'));
				return $this->redirect(array('controller'=>'packings', 'action' => 'view',$this->request->data['PackingDetail']['packing_id']));
			} else {
				$this->Session->setFlash(__('入庫詳細情報の更新に失敗しました。もう一度操作してください。'));
			}
		} else {
			$options = array('conditions' => array('PackingDetail.' . $this->PackingDetail->primaryKey => $id));
			$this->request->data = $this->PackingDetail->find('first', $options);
		}


		$this->setListValue();
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	function setListValue()
	{

		//コンテナ区分
		$containarOpt = array( '1'=>'ドライ','2'=>'リーファー');
		//荷送人
		$shipperOpt = $this->MtShipper->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//運送会社
		$carrierOpt = $this->MtCarrier->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//担当者
		$inchargeOpt = array( '1'=>'与那覇','2'=>'小宮');
		//商品名
		$sCdOpt = $this->MtSCd->find('list',array('fields'=>array('item_name'),'conditions'=>array('deleted is null')));
		//商品名
		$ksCdOpt = $this->MtSCd->find('list',array('fields'=>array('ks_cd'),'conditions'=>array('deleted is null')));
		//産地
		$areaOpt = $this->MtArea->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//規格
		$sizeOpt = $this->MtSize->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//単位
		$unitOpt = $this->MtUnit->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));

		$this->set(compact('containarOpt','shipperOpt','carrierOpt','inchargeOpt','ksCdOpt','sCdOpt','areaOpt','sizeOpt','unitOpt'));

	}

}

