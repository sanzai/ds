<?php
App::uses('AppController', 'Controller');
/**
 * IncomingDetails Controller
 *
 * @property IncomingDetail $IncomingDetail
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IncomingDetailsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	public $uses = array('Incoming', 'IncomingDetail','Packing', 'PackingDetail','MtCarrier','MtUnit','MtArea','MtShipper','MtSize','MtSCd','Outcoming','OutcomingDetail','setting');


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->IncomingDetail->recursive = 0;
		$this->set('incomingDetails', $this->Paginator->paginate());

		$this->setListValue();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->IncomingDetail->exists($id)) {
			throw new NotFoundException(__('不正なアクセスです。'));
		}
		$options = array('conditions' => array('IncomingDetail.' . $this->IncomingDetail->primaryKey => $id));
		$this->set('incomingDetail', $this->IncomingDetail->find('first', $options));

		$this->setListValue();
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->IncomingDetail->create();
			if ($this->IncomingDetail->save($this->request->data)) {
				$this->Session->setFlash(__('入庫詳細情報の登録が完了しました。'));

				return $this->redirect(array('controller'=>'Incomings','action' => 'view',$this->request->data['IncomingDetail']['incoming_id']));

			} else {
				$this->Session->setFlash(__('入庫詳細情報の登録に失敗しました。もう一度操作してください。'));
			}
		}
		$this->setListValue();
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->IncomingDetail->exists($id)) {
			throw new NotFoundException(__('不正なアクセスです。最初からやり直してください。'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->IncomingDetail->save($this->request->data)) {

				//$this->log($this->request->data['IncomingDetail']['incoming_id'],LOG_DEBUG);

				$this->Session->setFlash(__('入庫詳細情報の更新が完了しました。'));
				return $this->redirect(array('controller'=>'incomings', 'action' => 'view',$this->request->data['IncomingDetail']['incoming_id']));
			} else {
				$this->Session->setFlash(__('入庫詳細情報の更新に失敗しました。もう一度操作してください。'));
			}
		} else {
			$options = array('conditions' => array('IncomingDetail.' . $this->IncomingDetail->primaryKey => $id));
			$this->request->data = $this->IncomingDetail->find('first', $options);
		}
		$this->setListValue();
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->IncomingDetail->id = $id;
		if (!$this->IncomingDetail->exists()) {
			throw new NotFoundException(__('Invalid incoming detail'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->IncomingDetail->delete()) {
			$this->Session->setFlash(__('The incoming detail has been deleted.'));
		} else {
			$this->Session->setFlash(__('The incoming detail could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	function setListValue(){

		//コンテナ区分
		$containarOpt = array( '1'=>'ドライ','2'=>'リーファー');
		//荷送人
		$shipperOpt = $this->MtShipper->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//運送会社
		$carrierOpt = $this->MtCarrier->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//担当者
		$inchargeOpt = array( '1'=>'与那覇','2'=>'小宮');
		//商品名
		$sCdOpt = $this->MtSCd->find('list',array('fields'=>array('item_name'),'conditions'=>array('deleted is null')));
		//産地
		$areaOpt = $this->MtArea->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//規格
		$sizeOpt = $this->MtSize->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//単位
		$unitOpt = $this->MtUnit->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//$userOpt = $this->User->find('list',array('fields'=>array('user_name'),'conditions'=>array('deleted_at is null')));

		$this->set(compact('containarOpt','shipperOpt','carrierOpt','inchargeOpt','sCdOpt','areaOpt','sizeOpt','unitOpt'));

	}

}

