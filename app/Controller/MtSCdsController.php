<?php
App::uses('AppController', 'Controller');
/**
 * MtSCds Controller
 *
 * @property MtSCd $MtSCd
 * @property PaginatorComponent $Paginator
 */
class MtSCdsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	public $uses = array('MtSCd','User');

/**
 * index method
 *
 * @return void
 */
	public function index() {

        $this->paginate = array(
                    'limit' => 10,
        );
		$this->MtSCd->recursive = 0;
		$this->set('mtSCds', $this->Paginator->paginate());

		$this->setListValue();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MtSCd->exists($id)) {
			throw new NotFoundException(__('Invalid mt s cd'));
		}
		$options = array('conditions' => array('MtSCd.' . $this->MtSCd->primaryKey => $id));
		$this->set('mtSCd', $this->MtSCd->find('first', $options));

		$this->setListValue();
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MtSCd->create();
			if ($this->MtSCd->save($this->request->data)) {
				$this->Session->setFlash(__('The mt s cd has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mt s cd could not be saved. Please, try again.'));
			}
		}

		$this->setListValue();
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MtSCd->exists($id)) {
			throw new NotFoundException(__('Invalid mt s cd'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MtSCd->save($this->request->data)) {
				$this->Session->setFlash(__('The mt s cd has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mt s cd could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MtSCd.' . $this->MtSCd->primaryKey => $id));
			$this->request->data = $this->MtSCd->find('first', $options);
		}

		$this->setListValue();
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MtSCd->id = $id;
		if (!$this->MtSCd->exists()) {
			throw new NotFoundException(__('Invalid mt s cd'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MtSCd->delete()) {
			$this->Session->setFlash(__('The mt s cd has been deleted.'));
		} else {
			$this->Session->setFlash(__('The mt s cd could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	function setListValue(){

		//担当バイヤー
		$useridOpt = $this->User->find('list',array('fields'=>array('user_name'),'conditions'=>array('user_div = 2 and deleted is null')));
		//加工フラグ
		$packingflgOpt = array('0'=>'対象外','1'=>'対象');
		//商品コード
		$sCdOpt = array( '1'=>'人参','2'=>'玉葱','3'=>'じゃがいも');
		//規格
		$sizeOpt = array( '1'=>'S','2'=>'M','3'=>'L','２L');

		$this->set(compact('useridOpt','packingflgOpt','sCdOpt','sizeOpt'));

	}

}
