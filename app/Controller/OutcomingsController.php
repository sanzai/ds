<?php
App::uses('AppController', 'Controller');
/**
 * Outcomings Controller
 *
 * @property Outcoming $Outcoming
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class OutcomingsController extends AppController {




/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	public $uses = array('Outcoming','Incoming', 'IncomingDetail','Packing', 'PackingDetail','MtCarrier','MtUnit','MtArea','MtShipper','MtSize','MtSCd','OutcomingDetail','setting');


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Outcoming->recursive = 0;
		$this->set('outcomings', $this->Paginator->paginate());

		$this->setListValue();
	}


/**
 * index method
 *
 * @return void
 */
	public function import() {
		$this->Outcoming->recursive = 0;
		$this->set('outcomings', $this->Paginator->paginate());

		$this->setListValue();
	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Outcoming->exists($id)) {
			throw new NotFoundException(__('Invalid outcoming'));
		}
		$options = array('conditions' => array('Outcoming.' . $this->Outcoming->primaryKey => $id));
		$this->set('outcoming', $this->Outcoming->find('first', $options));

		$this->setListValue();
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Outcoming->create();
			if ($this->Outcoming->save($this->request->data)) {
				$this->Session->setFlash(__('The outcoming has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The outcoming could not be saved. Please, try again.'));
			}
		}

		$this->setListValue();
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Outcoming->exists($id)) {
			throw new NotFoundException(__('Invalid outcoming'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Outcoming->save($this->request->data)) {
				$this->Session->setFlash(__('The outcoming has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The outcoming could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Outcoming.' . $this->Outcoming->primaryKey => $id));
			$this->request->data = $this->Outcoming->find('first', $options);
		}

		$this->setListValue();
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Outcoming->id = $id;
		if (!$this->Outcoming->exists()) {
			throw new NotFoundException(__('Invalid outcoming'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Outcoming->delete()) {
			$this->Session->setFlash(__('The outcoming has been deleted.'));
		} else {
			$this->Session->setFlash(__('The outcoming could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	function setListValue(){

		//コンテナ区分
		$containarOpt = array( '1'=>'ドライ','2'=>'リーファー');
		//荷送人
		$shipperOpt = $this->MtShipper->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//運送会社
		$carrierOpt = $this->MtCarrier->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//担当者
		$inchargeOpt = array( '1'=>'与那覇','2'=>'小宮');
		//商品名
		$sCdOpt = $this->MtSCd->find('list',array('fields'=>array('item_name'),'conditions'=>array('deleted is null')));
		//産地
		$areaOpt = $this->MtArea->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//規格
		$sizeOpt = $this->MtSize->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//単位
		$unitOpt = $this->MtUnit->find('list',array('fields'=>array('disp_name'),'conditions'=>array('deleted is null')));
		//$userOpt = $this->User->find('list',array('fields'=>array('user_name'),'conditions'=>array('deleted_at is null')));

		$this->set(compact('containarOpt','shipperOpt','carrierOpt','inchargeOpt','sCdOpt','areaOpt','sizeOpt','unitOpt'));

	}


}

