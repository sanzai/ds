<div class="mtSizes view">
<h2><?php echo __('Mt Size'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mtSize['MtSize']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item Name'); ?></dt>
		<dd>
			<?php echo h($mtSize['MtSize']['item_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Disp Name'); ?></dt>
		<dd>
			<?php echo h($mtSize['MtSize']['disp_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($mtSize['MtSize']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($mtSize['MtSize']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($mtSize['MtSize']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($mtSize['MtSize']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($mtSize['MtSize']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($mtSize['MtSize']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mt Size'), array('action' => 'edit', $mtSize['MtSize']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mt Size'), array('action' => 'delete', $mtSize['MtSize']['id']), null, __('Are you sure you want to delete # %s?', $mtSize['MtSize']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Mt Sizes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mt Size'), array('action' => 'add')); ?> </li>
	</ul>
</div>
