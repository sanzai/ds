<br />

	<table style="float:center" width="100%">
		<tr>
			<td colspan="4" class="t-head">入庫情報詳細</td>
		</tr>
		<tr>
			<td width="100" class="s-head">入庫ID</td>
			<td width="250" class="data"><?php echo h($incoming['Incoming']['id']); ?></td>
			<td width="100" class="s-head">受付者</td>
			<td width="250" class="data"><?php echo h($inchargeOpt[$incoming['Incoming']['incharge']]); ?></td>
		</tr>
		<tr>
			<td class="s-head">入庫予定日</td>
			<td class="data"><?php echo h($incoming['Incoming']['incoming_plan']); ?></td>
			<td class="s-head">入庫日</td>
			<td class="data"><?php echo h($incoming['Incoming']['incoming_date']); ?></td>
		</tr>
		<tr>
			<td class="s-head">コンテナNo</td>
			<td class="data"><?php echo h($incoming['Incoming']['containar_no']); ?></td>
			<td class="s-head">コンテナ区分</td>
			<td class="data"><?php echo h($containarOpt[$incoming['Incoming']['containar_div']]); ?></td>
		</tr>
		<tr>
			<td class="s-head">荷送人</td>
			<td class="data"><?php echo h($shipperOpt[$incoming['Incoming']['shipper']]); ?></td>
			<td class="s-head">運送会社</td>
			<td class="data"><?php echo h($carrierOpt[$incoming['Incoming']['carrier']]); ?></td>
		</tr>
	</table>

 	<br />

	<div style="width:100%;" align="center">
		<button style="width:150px;height:30px;cursor:pointer" onclick="location.href='<?php echo $this->html->url('/incomings/edit/' . $incoming['Incoming']['id']);?>';">修正</button>
	</div>
<br>
	<table style="float:center" width="100%">


		<tr>
			<td colspan="8" class="t-head">入庫アイテム一覧</td>
		</tr>
		<tr>
			<td width="10%" class="s-head">入庫商品ＩＤ</td>
			<td width="15%" class="s-head">金秀コード</td>
			<td width="20%" class="s-head">商品名</td>
			<td width="20%" class="s-head">産地</td>
			<td width="10%" class="s-head">規格</td>
			<td width="10%" class="s-head">数量</td>
			<td width="10%" class="s-head">単位</td>
			<td width="5%" class="s-head">操作</td>
		</tr>
		<?php foreach ($incomingDetails as $incomingDetail): ?>
		<tr>
			<td class="data"><?php echo h($incomingDetail['IncomingDetail']['id']); ?></td>
			<td class="data"><?php echo h($incomingDetail['MtSCd']['ks_cd']); ?></td>
			<td class="data"><?php echo h($sCdOpt[$incomingDetail['IncomingDetail']['s_cd']]); ?></td>
			<td class="data"><?php echo h($areaOpt[$incomingDetail['IncomingDetail']['area_cd']]); ?></td>
			<td class="data_right"><?php echo h($sizeOpt[$incomingDetail['IncomingDetail']['size']]); ?></td>
			<td class="data_right"><?php echo number_format($incomingDetail['IncomingDetail']['amount']); ?></td>
			<td class="data_center"><?php echo h($unitOpt[$incomingDetail['IncomingDetail']['unit']]); ?></td>
			<td class="data_center">
				<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/incomingDetails/edit/' . $incomingDetail['IncomingDetail']['id']);?>';">修正</button>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>



	<br />

	<?php echo $this->Form->create('IncomingDetail',array('action' =>'add')); ?>
	<?php echo $this->Form->input('incoming_id',array('type'=>'hidden', 'value'=>$incoming['Incoming']['id']));?>
	<table style="float:center" width="100%">

		<br />
		<tr>
			<td colspan="8" class="t-head">入庫アイテム新規登録</td>
		</tr>
		<tr>
			<td class="s-head">商品名</td>
			<td class="data"><?php echo $this->Form->input('s_cd', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $sCdOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td class="s-head">産地</td>
			<td class="data"><?php echo $this->Form->input('area_cd', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $areaOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td class="s-head">規格</td>
			<td class="data"><?php echo $this->Form->input('size', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $sizeOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td class="s-head">数量</td>
			<td class="data"><?php echo $this->Form->input('amount', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
		<tr>
			<td class="s-head">単位</td>
			<td class="data"><?php echo $this->Form->input('unit', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $unitOpt, 'empty'=>'-----')); ?></td>
		</tr>
	</table>


	<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/incomings/index');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<?php echo $this->Form->submit('登　録', array('div'=>false, 'name' => 'add', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
	</div>
<?php echo $this->Form->end(); ?>

<br />
<br />

