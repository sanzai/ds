<script>
$(function() {
$( ".datepicker" ).datepicker();
});

function submitStop(e){
	if (!e) var e = window.event;

		if(e.keyCode == 13)
			return false;
		}

</script>

	<br />

<?php echo $this->Form->create('Incoming'); ?>
	<table style="float:center" width="100%">

		<br />
		<tr>
			<td colspan="8" class="t-head">入庫基本情報登録</td>
		</tr>
		<tr>
			<td width="140" class="s-head">受付者</td>
			<td width="340"  class="data"><?php echo $this->Form->input('incharge', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $inchargeOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td width="140" class="s-head">入庫予定日</td>
			<td width="340"  class="data"><?php echo $this->Form->input('incoming_plan', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'datepicker')); ?></td>
			<td width="140" class="s-head">入庫日</td>
			<td width="340"  class="data"><?php echo $this->Form->input('incoming_date', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'datepicker')); ?></td>
		</tr>
		<tr>
			<td class="s-head">コンテナＮｏ</td>
			<td class="data"><?php echo $this->Form->input('containar_no', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
			<td class="s-head">コンテナ区分</td>
			<td class="data"><?php echo $this->Form->input('containar_div', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $containarOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td class="s-head">荷送人</td>
			<td class="data"><?php echo $this->Form->input('shipper', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $shipperOpt, 'empty'=>'-----')); ?></td>
			<td class="s-head">運送会社</td>
			<td class="data"><?php echo $this->Form->input('carrier', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $carrierOpt, 'empty'=>'-----')); ?></td>
		</tr>
	</table>

	<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/incomings/index');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button> 
		<?php echo $this->Form->submit('登　録', array('div'=>false, 'name' => 'add', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
	</div>
<?php echo $this->Form->end(); ?>

<br />
<br />
<br />


<!--

<div class="incomings form">
<?php echo $this->Form->create('Incoming'); ?>
	<fieldset>
		<legend><?php echo __('Add Incoming'); ?></legend>
	<?php
		echo $this->Form->input('incharge');
		echo $this->Form->input('incoming_plan');
		echo $this->Form->input('incoming_date');
		echo $this->Form->input('containar_no');
		echo $this->Form->input('containar_div');
		echo $this->Form->input('shipper');
		echo $this->Form->input('carrier');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Incomings'), array('action' => 'index')); ?></li>
	</ul>
</div>
-->