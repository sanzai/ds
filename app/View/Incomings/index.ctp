

<script type="text/javascript">

$(function() {
$( ".datepicker" ).datepicker();
});

function submitStop(e){
	if (!e) var e = window.event;

		if(e.keyCode == 13)
			return false;
		}


function toAdd(){
	document.location = "<?php echo Router::url(array('controller'=>'incomings','action'=>'add'));?>";
}
function toClear(){
	document.location = "<?php echo Router::url(array('controller'=>'incomings','action'=>'index'));?>";
}
</script>

<br />

	<?php
		echo $this->Form->create('Incomings');
	?>
	<table style="float:center" width="100%">
		<tr>
			<td colspan="6" class="t-head-left" >検索条件</td>
		</tr>
		<tr>
			<td width="140" class="s-head">入庫日</td>
			<td width="240" class="data" colspan="3"><?php echo $this->Form->input('date_from', array('type' => 'text', 'div'=>false, 'label' => false, 'placeholder' => 'yyyy/mm/dd', 'class'=>'datepicker'));?>～<?php echo $this->Form->input('date_from', array('type' => 'text', 'div'=>false, 'label' => false, 'placeholder' => 'yyyy/mm/dd', 'class'=>'datepicker'));?></td>
			<td width="140" class="s-head">コンテナ区分</td>
			<td width="240" class="data"><?php echo $this->Form->input('s_contract_div',array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $containarOpt, 'empty'=>'-----')); ?></tr>
		<tr>
			<td width="140" class="s-head">荷送人</td>
			<td width="240" class="data"><?php echo $this->Form->input('shipper', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $shipperOpt, 'empty'=>'-----')); ?></td>
			<td width="140" class="s-head">運送会社</td>
			<td width="240" class="data"><?php echo $this->Form->input('carrier', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $carrierOpt, 'empty'=>'-----')); ?></td>
			<td width="140" class="s-head">受付者</td>
			<td width="240" class="data"><?php echo $this->Form->input('s_contract_div', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $inchargeOpt, 'empty'=>'-----')); ?></td>
		</tr>
	</table>
	<br>
	<div style="width:100%;" align="center">
		<?php echo $this->Form->submit('検　索', array('div'=>false, 'name' => 'searching', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
		<?php echo $this->Form->submit('クリア', array('div'=>false, 'name' => 'clear', 'type'=>'button', 'style'=>'width:150px;height:30px;cursor:pointer', 'onClick'=>'toClear()')); ?>
		<?php echo $this->Form->submit('新規登録', array('div'=>false, 'name' => 'add', 'type'=>'button', 'style'=>'width:150px;height:30px;cursor:pointer', 'onClick'=>'toAdd()')); ?>
	</div>
	<br>
	<?php echo $this->Form->end();?>

	<table style="float:center" width="100%">
		<tr>
			<td class="t-head-left">検索結果一覧</td>
			<td colspan="6" class="t-head">1/1ページ  1件目から3件目 検索結果3件 </td>
		</tr>

	<table style="float:center" width="100%">

		<tr>
			<td width="100" class="s-head">入庫ID</td>
			<td width="100" class="s-head">入庫予定日</td>
			<td width="100" class="s-head">入庫日</td>
			<td width="200" class="s-head">コンテナNo</td>
			<td width="100" class="s-head">コンテナ区分</td>
			<td width="250" class="s-head">荷送人</td>
			<td width="200" class="s-head">運送会社</td>
			<td width="120" class="s-head">受付者</td>
			<td width="100" class="s-head">詳細</td>
		</tr>
		<?php foreach ($incomings as $incoming): ?>
		<tr>
			<td  class="data"><?php echo h($incoming['Incoming']['id']); ?></td>
			<td  class="data"><?php echo h($incoming['Incoming']['incoming_plan']); ?></td>
			<td  class="data"><?php echo h($incoming['Incoming']['incoming_date']); ?></td>
			<td  class="data"><?php echo h($incoming['Incoming']['containar_no']); ?></td>
			<td  class="data"><?php echo h($containarOpt[$incoming['Incoming']['containar_div']]); ?></td>
			<td  class="data"><?php echo h($shipperOpt[$incoming['Incoming']['shipper']]); ?></td>
			<td  class="data"><?php echo h($carrierOpt[$incoming['Incoming']['carrier']]); ?></td>
			<td  class="data" align="center"><?php echo h($inchargeOpt[$incoming['Incoming']['incharge']]); ?></td>
			<td  class="data_center">
				<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/incomings/view/' . $incoming['Incoming']['id']);?>';">詳細</button>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<br />
	<div style="width:100%;" align="center">

	</div>

<br />
<br />
<br />



<!--
<div class="incomings index">
	<h2><?php echo __('Incomings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('incharge'); ?></th>
			<th><?php echo $this->Paginator->sort('incoming_plan'); ?></th>
			<th><?php echo $this->Paginator->sort('incoming_date'); ?></th>
			<th><?php echo $this->Paginator->sort('containar_no'); ?></th>
			<th><?php echo $this->Paginator->sort('containar_div'); ?></th>
			<th><?php echo $this->Paginator->sort('shipper'); ?></th>
			<th><?php echo $this->Paginator->sort('carrier'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('created_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted_user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($incomings as $incoming): ?>
	<tr>
		<td><?php echo h($incoming['Incoming']['id']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['incharge']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['incoming_plan']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['incoming_date']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['containar_no']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['containar_div']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['shipper']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['carrier']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['created']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['created_user_id']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['updated']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['updated_user_id']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($incoming['Incoming']['deleted_user_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $incoming['Incoming']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $incoming['Incoming']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $incoming['Incoming']['id']), null, __('Are you sure you want to delete # %s?', $incoming['Incoming']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Incoming'), array('action' => 'add')); ?></li>
	</ul>
</div>
-->