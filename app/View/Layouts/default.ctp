<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php //echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('style');
		echo $this->Html->css('default');
		echo $this->Html->css('south-street/jquery-ui-1.10.2.custom.min');
		
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>	
	<?php
		echo $this->Html->script('jquery-ui-1.10.2.custom.min');
		echo $this->Html->script('jquery.ui.datepicker-ja');
		echo $scripts_for_layout;
	?>
	
</head>
<body>

	<div id="page">

		<div id="header">
		<img src="<?php echo $this->webroot; ?>img/logo.png" width="280" />
		<!-- / #header --></div>

<?php
//u_auth = AuthComponent::user('user_auth');
?>


		<div id="navi">
		<div id="naviArea">
		<ul id="gNav">
		<li><a href="<?php echo $this->webroot; ?>stocks">在庫管理</a></li>
		<li><a href="<?php echo $this->webroot; ?>incomings">入庫管理</a></li>
		<li><a href="<?php echo $this->webroot; ?>outcomings">出庫管理</a></li>
		<li><a href="<?php echo $this->webroot; ?>packings">加工管理</a></li>
		<li><a href="<?php echo $this->webroot; ?>settings">設定管理</a></li>
		<li><a href="<?php echo $this->webroot; ?>users">ユーザ管理</a></li>
		<li><a href="<?php echo $this->webroot; ?>users/logout">ログアウト</a></li>
		</ul>
		<!-- / #naviArea --></div>
		<!-- / #navi --></div>

		<div id="contents">
			<div id="main">

				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>

			</div>
		</div>

		<div id="footer">
		<div class="copyright">Copyright &copy; 2015 Global Media Communications All Rights Reserved. </div>
		<!-- / #footer --></div>
		
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
