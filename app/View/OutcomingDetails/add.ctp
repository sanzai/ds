<div class="outcomingDetails form">
<?php echo $this->Form->create('OutcomingDetail'); ?>
	<fieldset>
		<legend><?php echo __('Add Outcoming Detail'); ?></legend>
	<?php
		echo $this->Form->input('outcoming_id');
		echo $this->Form->input('s_cd');
		echo $this->Form->input('order_amount');
		echo $this->Form->input('out_amount');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Outcoming Details'), array('action' => 'index')); ?></li>
	</ul>
</div>
