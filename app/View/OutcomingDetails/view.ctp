
<div class="outcomingDetails view">
<h2><?php echo __('Outcoming Detail'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($outcomingDetail['OutcomingDetail']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Outcoming Id'); ?></dt>
		<dd>
			<?php echo h($outcomingDetail['OutcomingDetail']['outcoming_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('S Cd'); ?></dt>
		<dd>
			<?php echo h($outcomingDetail['OutcomingDetail']['s_cd']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order Amount'); ?></dt>
		<dd>
			<?php echo h($outcomingDetail['OutcomingDetail']['order_amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Out Amount'); ?></dt>
		<dd>
			<?php echo h($outcomingDetail['OutcomingDetail']['out_amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($outcomingDetail['OutcomingDetail']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($outcomingDetail['OutcomingDetail']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($outcomingDetail['OutcomingDetail']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($outcomingDetail['OutcomingDetail']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($outcomingDetail['OutcomingDetail']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($outcomingDetail['OutcomingDetail']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Outcoming Detail'), array('action' => 'edit', $outcomingDetail['OutcomingDetail']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Outcoming Detail'), array('action' => 'delete', $outcomingDetail['OutcomingDetail']['id']), null, __('Are you sure you want to delete # %s?', $outcomingDetail['OutcomingDetail']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Outcoming Details'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Outcoming Detail'), array('action' => 'add')); ?> </li>
	</ul>
</div>
