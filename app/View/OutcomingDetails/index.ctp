<?php //var_dump($outcomingDetails); ?>

<p align="center">
<?php
    echo $this->Paginator->counter(array(
        'format' => __('{:page}/{:pages}ページ  {:start}件目から{:end} 件目 検索結果{:count}件')
        ));
?>
</p>
	<table style="float:center" width="100%">
		<tr>
			<td colspan="7" class="t-head">出庫情報詳細</td>
		</tr>

<table style="float:center" width="100%">
	<tr>
		<td width="10%" class="s-head">商品コード</td>
		<td width="30%" class="s-head">商品名</td>
		<td width="10%" class="s-head">発注数</td>
		<td width="10%" class="s-head">出庫数</td>
		<td width="10%" class="s-head">不足</td>
		<td width="10%" class="s-head">詳細</td>
	</tr>
<?php foreach ($outcomingDetails as $outcomingDetail): ?>
	<tr>
		<td class="data"><?php echo h($outcomingDetail['OutcomingDetail']['s_cd']); ?></td>
		<td class="data"><?php echo h($outcomingDetail['mt_s_cds']['item_name']); ?></td>
		<td class="data"><?php echo h($outcomingDetail['OutcomingDetail']['order_amount']); ?></td>
		<td class="data"><?php echo h($outcomingDetail['OutcomingDetail']['out_amount']); ?></td>
		<td class="data"><?php echo h($outcomingDetail['OutcomingDetail']['order_amount'])-($outcomingDetail['OutcomingDetail']['out_amount']); ?></td>
		<td  class="data_center">
			<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/outcomingDetails/edit/' . $outcomingDetail['OutcomingDetail']['id']);?>';">詳細</button>
		</td>
	</tr>
<?php endforeach; ?>
</table>

    <br />
    <div style="width:100%" align="center" class="paginateLinks">
        <?php echo $this->Paginator->first('<< ' . __('', true), array(), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->prev('前へ ' . __('', true), array(), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('次へ', true) . '', array(), null, array('class' => 'disabled'));?>
        <?php echo $this->Paginator->last(__('', true) . '>>', array(), null, array('class' => 'disabled'));?>
    </div>
	<br />	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/outcomings/index/');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
	</div>
	<br />



<!--
<div class="outcomingDetails index">
	<h2><?php echo __('Outcoming Details'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('outcoming_id'); ?></th>
			<th><?php echo $this->Paginator->sort('s_cd'); ?></th>
			<th><?php echo $this->Paginator->sort('order_amount'); ?></th>
			<th><?php echo $this->Paginator->sort('out_amount'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('created_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted_user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($outcomingDetails as $outcomingDetail): ?>
	<tr>
		<td><?php echo h($outcomingDetail['OutcomingDetail']['id']); ?>&nbsp;</td>
		<td><?php echo h($outcomingDetail['OutcomingDetail']['outcoming_id']); ?>&nbsp;</td>
		<td><?php echo h($outcomingDetail['OutcomingDetail']['s_cd']); ?>&nbsp;</td>
		<td><?php echo h($outcomingDetail['OutcomingDetail']['order_amount']); ?>&nbsp;</td>
		<td><?php echo h($outcomingDetail['OutcomingDetail']['out_amount']); ?>&nbsp;</td>
		<td><?php echo h($outcomingDetail['OutcomingDetail']['created']); ?>&nbsp;</td>
		<td><?php echo h($outcomingDetail['OutcomingDetail']['created_user_id']); ?>&nbsp;</td>
		<td><?php echo h($outcomingDetail['OutcomingDetail']['updated']); ?>&nbsp;</td>
		<td><?php echo h($outcomingDetail['OutcomingDetail']['updated_user_id']); ?>&nbsp;</td>
		<td><?php echo h($outcomingDetail['OutcomingDetail']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($outcomingDetail['OutcomingDetail']['deleted_user_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $outcomingDetail['OutcomingDetail']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $outcomingDetail['OutcomingDetail']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $outcomingDetail['OutcomingDetail']['id']), null, __('Are you sure you want to delete # %s?', $outcomingDetail['OutcomingDetail']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Outcoming Detail'), array('action' => 'add')); ?></li>
	</ul>
</div>
-->