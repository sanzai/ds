<div class="mtAreas view">
<h2><?php echo __('Mt Area'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mtArea['MtArea']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item Name'); ?></dt>
		<dd>
			<?php echo h($mtArea['MtArea']['item_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Disp Name'); ?></dt>
		<dd>
			<?php echo h($mtArea['MtArea']['disp_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($mtArea['MtArea']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($mtArea['MtArea']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($mtArea['MtArea']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($mtArea['MtArea']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($mtArea['MtArea']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($mtArea['MtArea']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mt Area'), array('action' => 'edit', $mtArea['MtArea']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mt Area'), array('action' => 'delete', $mtArea['MtArea']['id']), null, __('Are you sure you want to delete # %s?', $mtArea['MtArea']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Mt Areas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mt Area'), array('action' => 'add')); ?> </li>
	</ul>
</div>
