<p align="center">
<?php
    echo $this->Paginator->counter(array(
        'format' => __('{:page}/{:pages}ページ  {:start}件目から{:end} 件目 検索結果{:count}件')
        ));
?>
</p>
	<table style="float:center" width="100%">
		<tr>
			<td colspan="7" class="t-head-left">産地一覧</td>
		</tr>

	<table style="float:center" width="100%">

		<tr>
			<td width="20%" class="s-head">ID</td>
			<td width="20%" class="s-head">産地名</td>
			<td width="20%" class="s-head">表示名</td>
			<td width="20%" class="s-head">産地Ｆｌｇ</td>
			<td width="20%" class="s-head">操作</td>
		</tr>
		<?php foreach ($mtAreas as $mtArea): ?>
		<tr>
			<td  class="data"><?php echo h($mtArea['MtArea']['id']); ?></td>
			<td  class="data"><?php echo h($mtArea['MtArea']['item_name']); ?></td>
			<td  class="data"><?php echo h($mtArea['MtArea']['disp_name']); ?></td>
			<td  class="data_center"><?php echo h($dispflgOpt[$mtArea['MtArea']['disp_flg']]); ?></td>
			<td  class="data_center">
				<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/MtAreas/edit/' . $mtArea['MtArea']['id']);?>';">修正</button>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
    <br />
    <div style="width:100%" align="center" class="paginateLinks">
        <?php echo $this->Paginator->first('<< ' . __('', true), array(), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->prev('前へ ' . __('', true), array(), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('次へ', true) . '', array(), null, array('class' => 'disabled'));?>
        <?php echo $this->Paginator->last(__('', true) . '>>', array(), null, array('class' => 'disabled'));?>
    </div>
	<br />	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/settings/');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<button style="width:150px;height:30px;cursor:pointer" onclick="location.href='<?php echo $this->html->url('/MtAreas/add/');?>';">新規登録</button>
	</div>
<!--
	<div style="width:100%;" align="center">
		<button style="width:150px;height:30px;cursor:pointer" onclick="location.href='<?php echo $this->html->url('/MtAreas/add/');?>';">新規登録</button>
	</div>
-->
<br />
<br />
<br />



<!--
<div class="mtAreas index">
	<h2><?php echo __('Mt Areas'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('item_name'); ?></th>
			<th><?php echo $this->Paginator->sort('disp_name'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('created_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted_user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($mtAreas as $mtArea): ?>
	<tr>
		<td><?php echo h($mtArea['MtArea']['id']); ?>&nbsp;</td>
		<td><?php echo h($mtArea['MtArea']['item_name']); ?>&nbsp;</td>
		<td><?php echo h($mtArea['MtArea']['disp_name']); ?>&nbsp;</td>
		<td><?php echo h($mtArea['MtArea']['created']); ?>&nbsp;</td>
		<td><?php echo h($mtArea['MtArea']['created_user_id']); ?>&nbsp;</td>
		<td><?php echo h($mtArea['MtArea']['updated']); ?>&nbsp;</td>
		<td><?php echo h($mtArea['MtArea']['updated_user_id']); ?>&nbsp;</td>
		<td><?php echo h($mtArea['MtArea']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($mtArea['MtArea']['deleted_user_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $mtArea['MtArea']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $mtArea['MtArea']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mtArea['MtArea']['id']), null, __('Are you sure you want to delete # %s?', $mtArea['MtArea']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Mt Area'), array('action' => 'add')); ?></li>
	</ul>
</div>
-->