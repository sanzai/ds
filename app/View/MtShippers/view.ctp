<div class="mtShippers view">
<h2><?php echo __('Mt Shipper'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mtShipper['MtShipper']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item Name'); ?></dt>
		<dd>
			<?php echo h($mtShipper['MtShipper']['item_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Disp Name'); ?></dt>
		<dd>
			<?php echo h($mtShipper['MtShipper']['disp_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($mtShipper['MtShipper']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($mtShipper['MtShipper']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($mtShipper['MtShipper']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($mtShipper['MtShipper']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($mtShipper['MtShipper']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($mtShipper['MtShipper']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mt Shipper'), array('action' => 'edit', $mtShipper['MtShipper']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mt Shipper'), array('action' => 'delete', $mtShipper['MtShipper']['id']), null, __('Are you sure you want to delete # %s?', $mtShipper['MtShipper']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Mt Shippers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mt Shipper'), array('action' => 'add')); ?> </li>
	</ul>
</div>
