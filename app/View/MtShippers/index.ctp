
	<table style="float:center" width="100%">
		<tr>
			<td colspan="7" class="t-head-left">荷送人一覧</td>
		</tr>

	<table style="float:center" width="100%">

		<tr>
			<td width="100" class="s-head">ID</td>
			<td width="100" class="s-head">荷送人名</td>
			<td width="100" class="s-head">表示名</td>
			<td width="100" class="s-head">詳細</td>
		</tr>
		<?php foreach ($mtShippers as $mtShipper): ?>
		<tr>
			<td  class="data"><?php echo h($mtShipper['MtShipper']['id']); ?></td>
			<td  class="data"><?php echo h($mtShipper['MtShipper']['item_name']); ?></td>
			<td  class="data"><?php echo h($mtShipper['MtShipper']['disp_name']); ?></td>
			<td  class="data_center">
				<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/MtShippers/edit/' . $mtShipper['MtShipper']['id']);?>';">修正</button>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/settings/');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<button style="width:150px;height:30px;cursor:pointer" onclick="location.href='<?php echo $this->html->url('/MtShippers/add/');?>';">新規登録</button>
	</div>

<br />
<br />
<br />




<!--
<div class="mtShippers index">
	<h2><?php echo __('Mt Shippers'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('item_name'); ?></th>
			<th><?php echo $this->Paginator->sort('disp_name'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('created_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted_user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($mtShippers as $mtShipper): ?>
	<tr>
		<td><?php echo h($mtShipper['MtShipper']['id']); ?>&nbsp;</td>
		<td><?php echo h($mtShipper['MtShipper']['item_name']); ?>&nbsp;</td>
		<td><?php echo h($mtShipper['MtShipper']['disp_name']); ?>&nbsp;</td>
		<td><?php echo h($mtShipper['MtShipper']['created']); ?>&nbsp;</td>
		<td><?php echo h($mtShipper['MtShipper']['created_user_id']); ?>&nbsp;</td>
		<td><?php echo h($mtShipper['MtShipper']['updated']); ?>&nbsp;</td>
		<td><?php echo h($mtShipper['MtShipper']['updated_user_id']); ?>&nbsp;</td>
		<td><?php echo h($mtShipper['MtShipper']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($mtShipper['MtShipper']['deleted_user_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $mtShipper['MtShipper']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $mtShipper['MtShipper']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mtShipper['MtShipper']['id']), null, __('Are you sure you want to delete # %s?', $mtShipper['MtShipper']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Mt Shipper'), array('action' => 'add')); ?></li>
	</ul>
</div>
-->