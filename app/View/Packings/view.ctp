<?php //var_dump($packings); ?>
<br />

	<table style="float:center" width="100%">
		<tr>
			<td colspan="4" class="t-head">加工情報詳細</td>
		</tr>
		<tr>
			<td width="100" class="s-head">加工ＩＤ</td>
			<td width="250" class="data"><?php echo h($packing['Packing']['id']); ?></td>
		</tr>
		<tr>
			<td width="100" class="s-head">加工日</td>
			<td width="250" class="data"><?php echo h($packing['Packing']['packing_date']); ?></td>
			<td width="100" class="s-head">人数</td>
			<td width="250" class="data"><?php echo h($packing['Packing']['member']); ?></td>
		</tr>
	</table>
	<br />
	<div style="width:100%;" align="center">
		<button style="width:150px;height:30px;cursor:pointer" onclick="location.href='<?php echo $this->html->url('/packings/edit/' . $packing['Packing']['id']);?>';">修正</button>
	</div>
	<br>

	<table style="float:center" width="100%">
		<tr>
			<td colspan="11" class="t-head">加工情報詳細</td>
		</tr>
		<tr>
			<td width="5%" class="s-head">加工ＩＤ</td>
			<td width="16%" class="s-head">加工商品名</td>
			<td width="5%" class="s-head">規格</td>
			<td width="10%" class="s-head">利用数量</td>
			<td width="16%" class="s-head">加工商品名2</td>
			<td width="5%" class="s-head">規格2</td>
			<td width="10%" class="s-head">利用数量2</td>
			<td width="10%" class="s-head">出来高（ＣＳ）</td>
			<td width="10%" class="s-head">出来高（ＰＣ）</td>
			<td width="8%" class="s-head">ロス（ＫＧ）</td>
			<td width="5%" class="s-head">操作</td>
		</tr>
		<?php foreach ($packingDetails as $packingDetail): ?>
		<tr>
			<td class="data"><?php echo h($packingDetail['PackingDetail']['id']); ?></td>
			<td class="data"><?php echo h($sCdOpt[$packingDetail['IncomingDetail']['s_cd']]); ?></td>
			<td class="data_right"><?php echo h($sizeOpt[$packingDetail['IncomingDetail']['size']]); ?></td>
			<td class="data_right"><?php echo number_format($packingDetail['PackingDetail']['use_amount']); ?></td>
			<td class="data">
				<?php
				if(empty($packingDetail['PackingDetail']['s_cd2'])){
					echo "";
				}else{
					echo h($sCdOpt[$packingDetail['PackingDetail']['s_cd2']]);
				}
				?>
			</td>
			<td class="data_right">
				<?php
				if(empty($packingDetail['PackingDetail']['size2'])){
					echo "";
				}else{
					echo h($sizeOpt[$packingDetail['PackingDetail']['size2']]);
				}
				?>
			</td>
			<td class="data_right">
				<?php
				if(empty($packingDetail['PackingDetail']['use_amount2'])){
					echo "";
				}else{
					echo number_format($packingDetail['PackingDetail']['use_amount2']);
				}
				?>
			</td>
			<td class="data_right"><?php echo number_format($packingDetail['PackingDetail']['volume_cs']); ?></td>
			<td class="data_right"><?php echo number_format($packingDetail['PackingDetail']['volume_pc']); ?></td>
			<td class="data_right"><?php echo number_format($packingDetail['PackingDetail']['loss']); ?></td>

			<td class="data_center">
				<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/packingDetails/edit/' . $packingDetail['PackingDetail']['id']);?>';">修正</button>
			</td>
		<?php endforeach; ?>
	</table>
		<br />

	<br />

	<?php echo $this->Form->create('PackingDetail',array('action' =>'add')); ?>
	<?php echo $this->Form->input('packing_id',array('type'=>'hidden', 'value'=>$packing['Packing']['id']));?>
	<table style="float:center" width="100%">

		<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/packings/index');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
	</div>
<?php echo $this->Form->end(); ?>

<br />
<br />
<br />









	<!--
		<tr>
			<td class="data">0001-01</td>
			<td class="data">2015/09/11</td>
			<td class="data">50</td>
			<td class="data">48</td>
			<td class="data">0</td>
			<td class="data">2</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td class="data">0001-02</td>
			<td class="data">2015/09/12</td>
			<td class="data">100</td>
			<td class="data">0</td>
			<td class="data">150</td>
			<td class="data">50</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td class="data">0002-03</td>
			<td class="data">2015/09/15</td>
			<td class="data">100</td>
			<td class="data">90</td>
			<td class="data">0</td>
			<td class="data">10</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td class="data">0011-04</td>
			<td class="data">2015/09/17</td>
			<td class="data">200</td>
			<td class="data">0</td>
			<td class="data">200</td>
			<td class="data">0</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
	</table>
<br>



	<div style="width:100%;" align="center">
		<a href="process.html">
			<button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="">戻る</button>
		</a>
	</div>

<br />
<br />
<br />




<div class="packings view">
<h2><?php echo __('Packing'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($packing['Packing']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Incharge'); ?></dt>
		<dd>
			<?php echo h($packing['Packing']['incharge']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Packing Date'); ?></dt>
		<dd>
			<?php echo h($packing['Packing']['packing_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Member'); ?></dt>
		<dd>
			<?php echo h($packing['Packing']['member']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($packing['Packing']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($packing['Packing']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($packing['Packing']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($packing['Packing']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($packing['Packing']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($packing['Packing']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Packing'), array('action' => 'edit', $packing['Packing']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Packing'), array('action' => 'delete', $packing['Packing']['id']), null, __('Are you sure you want to delete # %s?', $packing['Packing']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Packings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Packing'), array('action' => 'add')); ?> </li>
	</ul>
</div>
-->