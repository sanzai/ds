<?php //var_dump($packings); ?>
<script type="text/javascript">

$(function() {
$( ".datepicker" ).datepicker();
});

function submitStop(e){
	if (!e) var e = window.event;

		if(e.keyCode == 13)
			return false;
		}


function toAdd(){
	document.location = "<?php echo Router::url(array('controller'=>'packings','action'=>'add'));?>";
}
function toClear(){
	document.location = "<?php echo Router::url(array('controller'=>'packings','action'=>'index'));?>";
}
</script>

<br />

	<?php
		echo $this->Form->create('Packings');
	?>
	<table style="float:center" width="100%">
		<tr>
			<td colspan="6" class="t-head-left" >検索条件</td>
		</tr>
	<tr>
			<td width="140" class="s-head">加工日</td>
			<td width="240" class="data" colspan="3"><?php echo $this->Form->input('date_from', array('type' => 'text', 'div'=>false, 'label' => false, 'placeholder' => 'yyyy/mm/dd', 'class'=>'datepicker'));?>～<?php echo $this->Form->input('date_from', array('type' => 'text', 'div'=>false, 'label' => false, 'placeholder' => 'yyyy/mm/dd', 'class'=>'datepicker'));?></td>

	</tr>
	</table>
			<br>
	<div style="width:100%;" align="center">
		<?php echo $this->Form->submit('検　索', array('div'=>false, 'name' => 'searching', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
		<?php echo $this->Form->submit('クリア', array('div'=>false, 'name' => 'clear', 'type'=>'button', 'style'=>'width:150px;height:30px;cursor:pointer', 'onClick'=>'toClear()')); ?>
		<?php echo $this->Form->submit('新規登録', array('div'=>false, 'name' => 'add', 'type'=>'button', 'style'=>'width:150px;height:30px;cursor:pointer', 'onClick'=>'toAdd()')); ?>
	</div>
	<br>
		<?php echo $this->Form->end();?>

	<table style="float:center" width="100%">
		<tr>
			<td class="t-head-left">検索結果一覧</td>
			<td colspan="6" class="t-head">1/1ページ  1件目から3件目 検索結果3件 </td>
		</tr>

	<table style="float:center" width="100%">
			<tr>
			<td width="100" class="s-head">加工日</td>
			<td width="200" class="s-head">人数</td>
			<td width="100" class="s-head">詳細</td>
			</tr>
	<?php foreach ($packings as $packing): ?>
	<tr>
		<td class="data"><?php echo h($packing['Packing']['packing_date']); ?>&nbsp;</td>
		<td class="data"><?php echo h($packing['Packing']['member']); ?>&nbsp;</td>
			<td  class="data_center">
				<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/packings/view/' . $packing['Packing']['id']);?>';">詳細</button>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<br />
	<div style="width:100%;" align="center">

	</div>

<br />
<br />
<br />




<!--
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Packing'), array('action' => 'add')); ?></li>
	</ul>
</div>
	<table style="float:center" width="100%">
		<tr>
			<td colspan="6" class="t-head-left" >検索条件</td>
		</tr>
		<tr>
			<td width="140" class="s-head">加工日</td>
			<td width="240" class="data" colspan="3"><input type="text" class="" >～<input type="text" class="" ></td>

	</table><br>

	<div style="width:100%;" align="center">
		<button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="document.location='/vessels';">検索</button>
		<button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="document.location='/vessels/edit/12';">クリア</button>
		<a href="process_add2.html"><button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="document.location='/vessels';">新規登録</button>
	</div><br>

	<table style="float:center" width="100%">
		<tr>
			<td class="t-head-left">検索結果一覧</td>
			<td colspan="8" class="t-head">1/1ページ  1件目から4件目 検索結果4件 </td>
		</tr>


		<tr>
			<td width="300" class="s-head">加工日</td>
			<td width="350" class="s-head">人数</td>
			<td width="350" class="s-head">詳細</td>
		</tr>
		<tr>
			<td class="data">2015/09/11</td>
			<td class="data">25</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td class="data">2015/09/12</td>
			<td class="data">25</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td class="data">2015/09/14</td>
			<td class="data">25</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td class="data">2015/09/15</td>
			<td class="data">25</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>


	</table>
	<br />
	<div style="width:100%;" align="center">

	</div>

<br />
<br />
<br />


<div class="packings index">
	<h2><?php echo __('Packings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('incharge'); ?></th>
			<th><?php echo $this->Paginator->sort('packing_date'); ?></th>
			<th><?php echo $this->Paginator->sort('member'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('created_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted_user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($packings as $packing): ?>
	<tr>
		<td><?php echo h($packing['Packing']['id']); ?>&nbsp;</td>
		<td><?php echo h($packing['Packing']['incharge']); ?>&nbsp;</td>
		<td><?php echo h($packing['Packing']['packing_date']); ?>&nbsp;</td>
		<td><?php echo h($packing['Packing']['member']); ?>&nbsp;</td>
		<td><?php echo h($packing['Packing']['created']); ?>&nbsp;</td>
		<td><?php echo h($packing['Packing']['created_user_id']); ?>&nbsp;</td>
		<td><?php echo h($packing['Packing']['updated']); ?>&nbsp;</td>
		<td><?php echo h($packing['Packing']['updated_user_id']); ?>&nbsp;</td>
		<td><?php echo h($packing['Packing']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($packing['Packing']['deleted_user_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $packing['Packing']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $packing['Packing']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $packing['Packing']['id']), null, __('Are you sure you want to delete # %s?', $packing['Packing']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Packing'), array('action' => 'add')); ?></li>
	</ul>
</div>
-->