<script>
$(function() {
$( ".datepicker" ).datepicker();
});

function submitStop(e){
	if (!e) var e = window.event;

		if(e.keyCode == 13)
			return false;
		}

</script>


<?php echo $this->Form->create('Packing'); ?>
<?php echo $this->Form->input('id',array('type'=>'hidden'));?>
	<table style="float:center" width="100%">

		<br />
		<tr>
			<td colspan="8" class="t-head">加工基本情報更新</td>
		</tr>

		<tr>
			<td width="100" class="s-head">加工ＩＤ</td>
			<td width="250" class="data"><?php echo $this->Form->input('packing_id', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
		<tr>
			<td width="100" class="s-head">加工日</td>
			<td width="250" class="data"><?php echo $this->Form->input('packing_date', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'datepicker')); ?></td>
			<td width="100" class="s-head">人数</td>
			<td width="250" class="data"><?php echo $this->Form->input('member', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
	</table>
	<br>

	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/packings/view/1');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<?php echo $this->Form->submit('登　録', array('div'=>false, 'name' => 'add', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
	</div>
<?php echo $this->Form->end(); ?>

<br />
<br />
<br />


<!--
<br />

	<table style="float:center" width="100%">
		<tr>
			<td colspan="7" class="t-head">加工詳細</td>
		</tr>
		<tr>
			<td width="100" class="s-head">加工ＩＤ</td>
			<td width="100" class="s-head">加工日</td>
			<td width="100" class="s-head">利用数量</td>
			<td width="100" class="s-head">出来高（ＣＳ）</td>
			<td width="100" class="s-head">出来高（ＰＣ）</td>
			<td width="100" class="s-head">ロス（ＫＧ）</td>
			<td width="100" class="s-head">操作</td>
		</tr>
		<tr>
			<td class="data">0001-01</td>
			<td class="data">2015/09/11</td>
			<td class="data">50</td>
			<td class="data">48</td>
			<td class="data">0</td>
			<td class="data">2</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td class="data">0001-02</td>
			<td class="data">2015/09/12</td>
			<td class="data">100</td>
			<td class="data">0</td>
			<td class="data">150</td>
			<td class="data">50</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td class="data">0002-03</td>
			<td class="data">2015/09/15</td>
			<td class="data">100</td>
			<td class="data">90</td>
			<td class="data">0</td>
			<td class="data">10</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td class="data">0011-04</td>
			<td class="data">2015/09/17</td>
			<td class="data">200</td>
			<td class="data">0</td>
			<td class="data">200</td>
			<td class="data">0</td>
			<td  class="data_center"><a href="process_list.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
	</table>
<br>



	<div style="width:100%;" align="center">
		<a href="process.html">
			<button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="">戻る</button>
		</a>
	</div>

<br />
<br />
<br />


<div class="packings form">
<?php echo $this->Form->create('Packing'); ?>
	<fieldset>
		<legend><?php echo __('Edit Packing'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('incharge');
		echo $this->Form->input('packing_date');
		echo $this->Form->input('member');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Packing.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Packing.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Packings'), array('action' => 'index')); ?></li>
	</ul>
</div>
-->