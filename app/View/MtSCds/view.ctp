<div class="mtSCds view">
<h2><?php echo __('Mt S Cd'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mtSCd['MtSCd']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item Name'); ?></dt>
		<dd>
			<?php echo h($mtSCd['MtSCd']['item_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Disp Name'); ?></dt>
		<dd>
			<?php echo h($mtSCd['MtSCd']['disp_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ks Cd'); ?></dt>
		<dd>
			<?php echo h($mtSCd['MtSCd']['ks_cd']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($mtSCd['MtSCd']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($mtSCd['MtSCd']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($mtSCd['MtSCd']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($mtSCd['MtSCd']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($mtSCd['MtSCd']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($mtSCd['MtSCd']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mt S Cd'), array('action' => 'edit', $mtSCd['MtSCd']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mt S Cd'), array('action' => 'delete', $mtSCd['MtSCd']['id']), null, __('Are you sure you want to delete # %s?', $mtSCd['MtSCd']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Mt S Cds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mt S Cd'), array('action' => 'add')); ?> </li>
	</ul>
</div>
