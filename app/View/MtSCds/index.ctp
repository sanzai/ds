<p align="center">
<?php
    echo $this->Paginator->counter(array(
        'format' => __('{:page}/{:pages}ページ  {:start}件目から{:end} 件目 検索結果{:count}件')
        ));
?>
</p>
	<table style="float:center" width="100%">
		<tr>
			<td colspan="7" class="t-head-left">商品コード一覧</td>
		</tr>

	<table style="float:center" width="100%">

		<tr>
			<td width="8%" class="s-head">ID</td>
			<td width="29%" class="s-head">商品名</td>
			<td width="29%" class="s-head">表示名</td>
			<td width="8%" class="s-head">金秀コード</td>
			<td width="10%" class="s-head">担当バイヤー</td>
			<td width="8%" class="s-head">加工Ｆｌｇ</td>
			<td width="8%" class="s-head">詳細</td>
		</tr>
		<?php foreach ($mtSCds as $mtSCd): ?>
		<tr>
			<td  class="data"><?php echo h($mtSCd['MtSCd']['id']); ?></td>
			<td  class="data"><?php echo h($mtSCd['MtSCd']['item_name']); ?></td>
			<td  class="data"><?php echo h($mtSCd['MtSCd']['disp_name']); ?></td>
			<td  class="data"><?php echo h($mtSCd['MtSCd']['ks_cd']); ?></td>
			<td  class="data_center"><?php echo h($useridOpt[$mtSCd['MtSCd']['user_id']]); ?></td>
			<td  class="data_center"><?php echo h($packingflgOpt[$mtSCd['MtSCd']['packing_flg']]); ?></td>
			<td  class="data_center">
				<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/MtSCds/edit/' . $mtSCd['MtSCd']['id']);?>';">修正</button>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
    <br />
    <div style="width:100%" align="center" class="paginateLinks">
        <?php echo $this->Paginator->first('<< ' . __('', true), array(), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->prev('前へ ' . __('', true), array(), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('次へ', true) . '', array(), null, array('class' => 'disabled'));?>
        <?php echo $this->Paginator->last(__('', true) . '>>', array(), null, array('class' => 'disabled'));?>
    </div>
	<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/settings/');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<button style="width:150px;height:30px;cursor:pointer" onclick="location.href='<?php echo $this->html->url('/MtSCds/add/');?>';">新規登録</button>
	</div>
<br />
<br />
<br />




<!--
<div class="mtSCds index">
	<h2><?php echo __('Mt S Cds'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('item_name'); ?></th>
			<th><?php echo $this->Paginator->sort('disp_name'); ?></th>
			<th><?php echo $this->Paginator->sort('ks_cd'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('created_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted_user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($mtSCds as $mtSCd): ?>
	<tr>
		<td><?php echo h($mtSCd['MtSCd']['id']); ?>&nbsp;</td>
		<td><?php echo h($mtSCd['MtSCd']['item_name']); ?>&nbsp;</td>
		<td><?php echo h($mtSCd['MtSCd']['disp_name']); ?>&nbsp;</td>
		<td><?php echo h($mtSCd['MtSCd']['ks_cd']); ?>&nbsp;</td>
		<td><?php echo h($mtSCd['MtSCd']['created']); ?>&nbsp;</td>
		<td><?php echo h($mtSCd['MtSCd']['created_user_id']); ?>&nbsp;</td>
		<td><?php echo h($mtSCd['MtSCd']['updated']); ?>&nbsp;</td>
		<td><?php echo h($mtSCd['MtSCd']['updated_user_id']); ?>&nbsp;</td>
		<td><?php echo h($mtSCd['MtSCd']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($mtSCd['MtSCd']['deleted_user_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $mtSCd['MtSCd']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $mtSCd['MtSCd']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mtSCd['MtSCd']['id']), null, __('Are you sure you want to delete # %s?', $mtSCd['MtSCd']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Mt S Cd'), array('action' => 'add')); ?></li>
	</ul>
</div>
-->