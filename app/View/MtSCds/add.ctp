<?php echo $this->Form->create('MtSCd'); ?>
	<table style="float:center" width="100%">

		<br />
		<tr>
			<td colspan="8" class="t-head">商品コード新規登録</td>
		</tr>
		<tr>
			<td width="140" class="s-head">商品名</td>
			<td width="340"  class="data"><?php echo $this->Form->input('item_name', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
			<td width="140" class="s-head">表示名</td>
			<td width="340"  class="data"><?php echo $this->Form->input('disp_name', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
		<tr>
			<td width="140" class="s-head">商品コード名</td>
			<td width="340"  class="data"><?php echo $this->Form->input('ks_cd', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
			<td width="140" class="s-head">担当バイヤー</td>
			<td width="340"  class="data"><?php echo $this->Form->input('user_id', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $useridOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td width="140" class="s-head">加工Flg</td>
			<td width="340"  class="data"><?php echo $this->Form->input('packing_flg', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $packingflgOpt, 'empty'=>'-----')); ?></td>
		</tr>
		</table>

	<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/MtSCds/index');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<?php echo $this->Form->submit('登　録', array('div'=>false, 'name' => 'add', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
	</div>
<?php echo $this->Form->end(); ?>

<br />
<br />
<br />





<!--
<div class="mtSCds form">
<?php echo $this->Form->create('MtSCd'); ?>
	<fieldset>
		<legend><?php echo __('Add Mt S Cd'); ?></legend>
	<?php
		echo $this->Form->input('item_name');
		echo $this->Form->input('disp_name');
		echo $this->Form->input('ks_cd');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Mt S Cds'), array('action' => 'index')); ?></li>
	</ul>
</div>
-->