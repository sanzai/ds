<div class="orders form">
<?php echo $this->Form->create('Order'); ?>
	<fieldset>
		<legend><?php echo __('Add Order'); ?></legend>
	<?php
		echo $this->Form->input('out_date');
		echo $this->Form->input('s_cd');
		echo $this->Form->input('shop_cd');
		echo $this->Form->input('amount');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Orders'), array('action' => 'index')); ?></li>
	</ul>
</div>
