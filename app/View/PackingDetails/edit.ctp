<?php //var_dump($this->data); ?>

	<?php echo $this->Form->create('PackingDetail',array('action' =>'edit')); ?>
	<?php echo $this->Form->input('id',array('type'=>'hidden', 'value'=>$this->data['PackingDetail']['id']));?>
	<?php echo $this->Form->input('packing_id',array('type'=>'hidden', 'value'=>$this->data['PackingDetail']['packing_id']));?>
	<table style="float:center" width="100%">

		<br />
		<tr>
			<td colspan="8" class="t-head">加工アイテム登録</td>
		</tr>
		<tr>
			<td width="200" class="s-head">加工商品</td>
			<td class="data"><?php echo $this->Form->input('s_cd', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $sCdOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td width="200" class="s-head">規格</td>
			<td class="data"><?php echo $this->Form->input('size', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $sizeOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td class="s-head">利用数量</td>
			<td class="data"><?php echo $this->Form->input('use_amount', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
		<tr>
			<td width="200" class="s-head">加工商品2</td>
			<td class="data"><?php echo $this->Form->input('s_cd2', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $sCdOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td width="200" class="s-head">規格2</td>
			<td class="data"><?php echo $this->Form->input('size2', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $sizeOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td class="s-head">利用数量2</td>
			<td class="data"><?php echo $this->Form->input('use_amount2', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
		<tr>
			<td class="s-head">出来高（ＣＳ）</td>
			<td class="data"><?php echo $this->Form->input('volume_cs', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
		<tr>
			<td class="s-head">出来高（ＰＣ）</td>
			<td class="data"><?php echo $this->Form->input('volume_pc', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
		<tr>
			<td class="s-head">ロス（ＫＧ）</td>
			<td class="data"><?php echo $this->Form->input('loss', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
	</table>


	<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/packings/index');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<?php echo $this->Form->submit('登録', array('div'=>false, 'name' => 'add', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
	</div>
<?php echo $this->Form->end(); ?>

<br />




<!--
<div class="packingDetails form">
<?php echo $this->Form->create('PackingDetail'); ?>
	<fieldset>
		<legend><?php echo __('Edit Packing Detail'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('packing_id');
		echo $this->Form->input('incoming_details_id');
		echo $this->Form->input('use_amount');
		echo $this->Form->input('volume_cs');
		echo $this->Form->input('volume_pc');
		echo $this->Form->input('loss');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('PackingDetail.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('PackingDetail.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Packing Details'), array('action' => 'index')); ?></li>
	</ul>
</div>
-->