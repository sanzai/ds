<?php //echo var_dump($packingDetails); ?>
<?php //echo var_dump($incomingDetails); ?>

	<br />
	<table style="float:center" width="100%">
		<tr>
			<td colspan="10" class="t-head">加工商品詳細</td>
		</tr>
		<tr>
			<td width="8%" class="s-head">金秀コード</td>
			<td width="15%" class="s-head">商品名</td>
			<td width="10%" class="s-head">産地</td>
			<td width="10%" class="s-head">数量</td>
		</tr>
		<tr>
			<td class="data"><?php echo h($ksCdOpt[$incomingDetails['IncomingDetail']['s_cd']]); ?></td>
			<td class="data"><?php echo h($sCdOpt[$incomingDetails['IncomingDetail']['s_cd']]); ?></td>
			<td class="data"><?php echo h($areaOpt[$incomingDetails['IncomingDetail']['area_cd']]); ?></td>
			<td class="data"><?php echo h($incomingDetails['IncomingDetail']['amount']); ?></td>
		</tr>
	</table>
	<br />


	<table style="float:center" width="100%">
		<tr>
			<td colspan="10" class="t-head">加工履歴一覧</td>
		</tr>
		<tr>
			<td width="10%" class="s-head">加工ID</td>
			<td width="8%" class="s-head">金秀コード</td>
			<td width="15%" class="s-head">商品名</td>
			<td width="10%" class="s-head">産地</td>
			<td width="10%" class="s-head">加工日</td>
			<td width="10%" class="s-head">数量</td>
			<td width="10%" class="s-head">出来高（ＰＣ）</td>
			<td width="10%" class="s-head">出来高（ＣＳ）</td>
			<td width="10%" class="s-head">ロス</td>
			<td width="10%" class="s-head">操作</td>
		</tr>
		<?php foreach ($packingDetails as $packingDetail): ?>
		<tr>
			<td class="data"><?php echo h($packingDetail['PackingDetail']['id']); ?></td>
			<td class="data"><?php echo h($packingDetail['MtSCd']['ks_cd']); ?></td>
			<td class="data"><?php echo h($sCdOpt[$packingDetail['IncomingDetail']['s_cd']]); ?></td>
			<td class="data"><?php echo h($areaOpt[$packingDetail['IncomingDetail']['area_cd']]); ?></td>
			<td class="data"><?php echo h($packingDetail['Packing']['packing_date']); ?></td>
			<td class="data_right"><?php echo h($packingDetail['PackingDetail']['use_amount']); ?></td>
			<td class="data_right"><?php echo number_format($packingDetail['PackingDetail']['volume_cs']); ?></td>
			<td class="data_center"><?php echo h($packingDetail['PackingDetail']['volume_pc']); ?></td>
			<td class="data_center"><?php echo h($packingDetail['PackingDetail']['loss']); ?></td>
			<td class="data_center">
				<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/packingDetails/edit/' . $packingDetail['PackingDetail']['id']);?>';">修正</button>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>



	<br />


<!--

<div class="packingDetails view">
<h2><?php echo __('Packing Detail'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Packing Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['packing_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Incoming Details Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['incoming_details_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Use Amount'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['use_amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Volume Cs'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['volume_cs']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Volume Pc'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['volume_pc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Loss'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['loss']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Packing Detail'), array('action' => 'edit', $packingDetail['PackingDetail']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Packing Detail'), array('action' => 'delete', $packingDetail['PackingDetail']['id']), null, __('Are you sure you want to delete # %s?', $packingDetail['PackingDetail']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Packing Details'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Packing Detail'), array('action' => 'add')); ?> </li>
	</ul>
</div>
-->