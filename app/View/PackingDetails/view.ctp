<div class="packingDetails view">
<h2><?php echo __('Packing Detail'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Packing Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['packing_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Incoming Details Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['incoming_details_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Use Amount'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['use_amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Volume Cs'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['volume_cs']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Volume Pc'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['volume_pc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Loss'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['loss']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($packingDetail['PackingDetail']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Packing Detail'), array('action' => 'edit', $packingDetail['PackingDetail']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Packing Detail'), array('action' => 'delete', $packingDetail['PackingDetail']['id']), null, __('Are you sure you want to delete # %s?', $packingDetail['PackingDetail']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Packing Details'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Packing Detail'), array('action' => 'add')); ?> </li>
	</ul>
</div>
