
<?php //echo var_dump($incomingDetail); ?>


<?php echo $this->Form->create('PackingDetail'); ?>
<?php echo $this->Form->input('incoming_details_id',array('type'=>'hidden', 'value'=>$incoming_detail_id));?>
<?php echo $this->Form->input('packing_id',array('type'=>'hidden', 'value'=>$packing_id));?>

	<table style="float:center" width="100%">

		<br />
		<tr>
			<td colspan="8" class="t-head">加工詳細情報登録</td>
		</tr>
		<tr>
			<td width="140" class="s-head">加工商品</td>
			<td width="340"  class="data"><?php echo $incomingDetail['MtSCd']['item_name'] ?></td>
			<td width="140" class="s-head">加工日</td>
			<td width="340"  class="data"><?php echo $packing_date ?></td>
		</tr>
		<tr>
			<td width="140" class="s-head">使用数</td>
			<td width="340"  class="data"><?php echo $this->Form->input('use_amount', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
		<tr>
			<td class="s-head">出来高（ＰＣ）</td>
			<td class="data"><?php echo $this->Form->input('volume_pc', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
			<td class="s-head">出来高（ＣＳ）</td>
			<td class="data"><?php echo $this->Form->input('volume_cs', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
		<tr>
			<td class="s-head">ロス</td>
			<td class="data"><?php echo $this->Form->input('loss', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
	</table>

	<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/incomings/index');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button> 
		<?php echo $this->Form->submit('登　録', array('div'=>false, 'name' => 'add', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
	</div>
<?php echo $this->Form->end(); ?>

<br />
<br />
<br />





<!--

<div class="packingDetails form">
<?php echo $this->Form->create('PackingDetail'); ?>
	<fieldset>
		<legend><?php echo __('Add Packing Detail'); ?></legend>
	<?php
		echo $this->Form->input('packing_id');
		echo $this->Form->input('incoming_details_id');
		echo $this->Form->input('use_amount');
		echo $this->Form->input('volume_cs');
		echo $this->Form->input('volume_pc');
		echo $this->Form->input('loss');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Packing Details'), array('action' => 'index')); ?></li>
	</ul>
</div>
-->