
<div class="packingDetails index">
	<h2><?php echo __('Packing Details'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('packing_id'); ?></th>
			<th><?php echo $this->Paginator->sort('incoming_details_id'); ?></th>
			<th><?php echo $this->Paginator->sort('use_amount'); ?></th>
			<th><?php echo $this->Paginator->sort('volume_cs'); ?></th>
			<th><?php echo $this->Paginator->sort('volume_pc'); ?></th>
			<th><?php echo $this->Paginator->sort('loss'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('created_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted_user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($packingDetails as $packingDetail): ?>
	<tr>
		<td><?php echo h($packingDetail['PackingDetail']['id']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['packing_id']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['incoming_details_id']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['use_amount']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['volume_cs']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['volume_pc']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['loss']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['created']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['created_user_id']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['updated']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['updated_user_id']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($packingDetail['PackingDetail']['deleted_user_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $packingDetail['PackingDetail']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $packingDetail['PackingDetail']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $packingDetail['PackingDetail']['id']), null, __('Are you sure you want to delete # %s?', $packingDetail['PackingDetail']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Packing Detail'), array('action' => 'add')); ?></li>
	</ul>
</div>
