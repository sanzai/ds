<div class="incomingDetails view">
<h2><?php echo __('Incoming Detail'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Incoming Id'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['incoming_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Incoming Details Id'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['incoming_details_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('S Cd'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['s_cd']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Area Cd'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['area_cd']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Size'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['size']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Unit'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['unit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($incomingDetail['IncomingDetail']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Incoming Detail'), array('action' => 'edit', $incomingDetail['IncomingDetail']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Incoming Detail'), array('action' => 'delete', $incomingDetail['IncomingDetail']['id']), null, __('Are you sure you want to delete # %s?', $incomingDetail['IncomingDetail']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Incoming Details'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Incoming Detail'), array('action' => 'add')); ?> </li>
	</ul>
</div>
