<div class="incomingDetails form">
<?php echo $this->Form->create('IncomingDetail'); ?>
	<fieldset>
		<legend><?php echo __('Add Incoming Detail'); ?></legend>
	<?php
		echo $this->Form->input('incoming_id');
		echo $this->Form->input('incoming_details_id');
		echo $this->Form->input('s_cd');
		echo $this->Form->input('area_cd');
		echo $this->Form->input('size');
		echo $this->Form->input('amount');
		echo $this->Form->input('unit');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Incoming Details'), array('action' => 'index')); ?></li>
	</ul>
</div>
