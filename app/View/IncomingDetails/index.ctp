<div class="incomingDetails index">
	<h2><?php echo __('Incoming Details'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('incoming_id'); ?></th>
			<th><?php echo $this->Paginator->sort('incoming_details_id'); ?></th>
			<th><?php echo $this->Paginator->sort('s_cd'); ?></th>
			<th><?php echo $this->Paginator->sort('area_cd'); ?></th>
			<th><?php echo $this->Paginator->sort('size'); ?></th>
			<th><?php echo $this->Paginator->sort('amount'); ?></th>
			<th><?php echo $this->Paginator->sort('unit'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('created_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted_user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($incomingDetails as $incomingDetail): ?>
	<tr>
		<td><?php echo h($incomingDetail['IncomingDetail']['id']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['incoming_id']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['incoming_details_id']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['s_cd']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['area_cd']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['size']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['amount']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['unit']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['created']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['created_user_id']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['updated']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['updated_user_id']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($incomingDetail['IncomingDetail']['deleted_user_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $incomingDetail['IncomingDetail']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $incomingDetail['IncomingDetail']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $incomingDetail['IncomingDetail']['id']), null, __('Are you sure you want to delete # %s?', $incomingDetail['IncomingDetail']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Incoming Detail'), array('action' => 'add')); ?></li>
	</ul>
</div>
