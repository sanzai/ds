<?php //var_dump($this->data); ?>

	<?php echo $this->Form->create('IncomingDetail',array('action' =>'edit')); ?>
	<?php echo $this->Form->input('id',array('type'=>'hidden', 'value'=>$this->data['IncomingDetail']['id']));?>
	<?php echo $this->Form->input('incoming_id',array('type'=>'hidden', 'value'=>$this->data['IncomingDetail']['incoming_id']));?>
	<table style="float:center" width="100%">

		<br />
		<tr>
			<td colspan="8" class="t-head">入庫アイテム修正登録</td>
		</tr>
		<tr>
			<td class="s-head">商品名</td>
			<td class="data"><?php echo $this->Form->input('s_cd', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $sCdOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td class="s-head">産地</td>
			<td class="data"><?php echo $this->Form->input('area_cd', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $areaOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td class="s-head">規格</td>
			<td class="data"><?php echo $this->Form->input('size', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $sizeOpt, 'empty'=>'-----')); ?></td>
		</tr>
		<tr>
			<td class="s-head">数量</td>
			<td class="data"><?php echo $this->Form->input('amount', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
		<tr>
			<td class="s-head">単位</td>
			<td class="data"><?php echo $this->Form->input('unit', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $unitOpt, 'empty'=>'-----')); ?></td>
		</tr>
	</table>


	<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/incomings/index');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<?php echo $this->Form->submit('更　新', array('div'=>false, 'name' => 'edit', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
	</div>
<?php echo $this->Form->end(); ?>

<br />

<!--

<div class="incomingDetails form">
<?php echo $this->Form->create('IncomingDetail'); ?>
	<fieldset>
		<legend><?php echo __('Edit Incoming Detail'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('incoming_id');
		echo $this->Form->input('incoming_details_id');
		echo $this->Form->input('s_cd');
		echo $this->Form->input('area_cd');
		echo $this->Form->input('size');
		echo $this->Form->input('amount');
		echo $this->Form->input('unit');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('IncomingDetail.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('IncomingDetail.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Incoming Details'), array('action' => 'index')); ?></li>
	</ul>
</div>
-->