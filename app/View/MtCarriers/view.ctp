<div class="mtCarriers view">
<h2><?php echo __('Mt Carrier'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mtCarrier['MtCarrier']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item Name'); ?></dt>
		<dd>
			<?php echo h($mtCarrier['MtCarrier']['item_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Disp Name'); ?></dt>
		<dd>
			<?php echo h($mtCarrier['MtCarrier']['disp_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($mtCarrier['MtCarrier']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($mtCarrier['MtCarrier']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($mtCarrier['MtCarrier']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($mtCarrier['MtCarrier']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($mtCarrier['MtCarrier']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($mtCarrier['MtCarrier']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mt Carrier'), array('action' => 'edit', $mtCarrier['MtCarrier']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mt Carrier'), array('action' => 'delete', $mtCarrier['MtCarrier']['id']), null, __('Are you sure you want to delete # %s?', $mtCarrier['MtCarrier']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Mt Carriers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mt Carrier'), array('action' => 'add')); ?> </li>
	</ul>
</div>
