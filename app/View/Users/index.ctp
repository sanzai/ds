<script type="text/javascript">

$(function() {
$( ".datepicker" ).datepicker();
});

function submitStop(e){
	if (!e) var e = window.event;

		if(e.keyCode == 13)
			return false;
		}


function toAdd(){
	document.location = "<?php echo Router::url(array('controller'=>'users','action'=>'add'));?>";
}
function toClear(){
	document.location = "<?php echo Router::url(array('controller'=>'users','action'=>'index'));?>";
}
</script>

<br />
	<table style="float:center" width="100%">
		<tr>
			<td class="t-head-left">ユーザ一覧</td>
		</tr>

	<table style="float:center" width="100%">
			<tr>
			<td width="150" class="s-head">ユーザＩＤ</td>
			<td width="200" class="s-head">ユーザ名</td>
			<td width="200" class="s-head">ユーザ区分</td>
			<td width="100" class="s-head">権限</td>
			<td width="100" class="s-head">詳細</td>
			</tr>
	<?php foreach ($users as $user): ?>
	<tr>
		<td class="data"><?php echo h($user['User']['id']); ?>&nbsp;</td>
		<td class="data"><?php echo h($user['User']['user_name']); ?>&nbsp;</td>
		<td class="data"><?php echo h($userOpt[$user['User']['user_div']]); ?>&nbsp;</td>
		<td class="data"><?php echo h($authOpt[$user['User']['auth']]); ?>&nbsp;</td>
			<td  class="data_center">
				<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/Users/edit/' . $user['User']['id']);?>';">修正</button>
			</td>
	</tr>
		<?php endforeach; ?>
	</table>
	<br />
	<div style="width:100%;" align="center">
		<?php echo $this->Form->submit('新規登録', array('div'=>false, 'name' => 'add', 'type'=>'button', 'style'=>'width:150px;height:30px;cursor:pointer', 'onClick'=>'toAdd()')); ?>

	</div>

<br />
<br />
<br />



<!--
<div id="tabmenu">

</div>

<br />
<br />

	<table style="float:center" width="100%">
		<tr>
			<td colspan="5" class="t-head-left">ユーザ一覧</td>
		</tr>
		<tr>
			<td width="10%" class="s-head">ユーザID</td>
			<td width="40%" class="s-head">ユーザ名</td>
			<td width="30%" class="s-head">ユーザ区分</td>
			<td width="10%" class="s-head">権限</td>
			<td width="10%" class="s-head">詳細</td>
		</tr>
		<tr>
			<td  class="data_center">10001</td>
			<td  class="data">宮崎あおい</td>
			<td  class="data">金秀商事</td>
			<td  class="data_center">管理者</td>
			<td  class="data_center"><a href="user_detail.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td  class="data_center">20001</td>
			<td  class="data">永作博美</td>
			<td  class="data">DiamondStars</td>
			<td  class="data_center">一般</td>
			<td  class="data_center"><a href="user_detail.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
	</table>

<br />
<br />
<br />
<div class="users index">
	<h2><?php echo __('Users'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_name'); ?></th>
			<th><?php echo $this->Paginator->sort('user_div'); ?></th>
			<th><?php echo $this->Paginator->sort('auth'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('created_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted_user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['user_name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['user_div']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['auth']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['created']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['created_user_id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['updated']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['updated_user_id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['deleted_user_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?></li>
	</ul>
</div>
-->