<script>
$(function() {
$( ".datepicker" ).datepicker();
});

function submitStop(e){
	if (!e) var e = window.event;

		if(e.keyCode == 13)
			return false;
		}

</script>

	<br />

<?php echo $this->Form->create('User'); ?>
	<table style="float:center" width="100%">

		<br />
		<tr>
			<td colspan="8" class="t-head">ユーザ情報登録</td>
		</tr>
		<tr>
			<td width="140" class="s-head">ユーザＩＤ</td>
			<td width="340"  class="data"><?php echo $this->Form->input('id', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $inchargeOpt, 'empty'=>'-----')); ?></td>
			<td width="140" class="s-head">ユーザ名</td>
			<td width="340"  class="data"><?php echo $this->Form->input('user_name', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'datepicker')); ?></td>
		</tr>
		<tr>
			<td width="140" class="s-head">ログインＩＤ</td>
			<td width="340"  class="data"><?php echo $this->Form->input('log_id', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'datepicker')); ?></td>
			<td class="s-head">パスワード</td>
			<td class="data"><?php echo $this->Form->input('passwerd', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>

	</table>

	<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/users/index');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<?php echo $this->Form->submit('登　録', array('div'=>false, 'name' => 'view', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
	</div>
<?php echo $this->Form->end(); ?>

<br />
<br />
<br />




<!--
<div id="tabmenu">

</div>

<br />
<br />

	<table style="float:center" width="100%">
		<tr>
			<td width="120" class="s-head">ユーザＩＤ</td>
			<td width="220" class="data"><input type="text" class="if" value="1" /></td>
			<td width="120" class="s-head">Active</td>
			<td width="220" class="data"><select><option>yes</option><option>no</option></select></td>
		</tr>
		<tr>
			<td width="120" class="s-head">ユーザ名</td>
			<td width="220" class="data"><input type="text" class="if" value="Miyazaki Aoi" /></td>
		</tr>
		<tr>
			<td width="120" class="s-head">ログインＩＤ</td>
			<td width="220" class="data"><input type="text" class="if" value="miyazaki" /></td>
			<td width="120" class="s-head">パスワード</td>
			<td width="220" class="data"><input type="text" class="if" value="password" /></td>
		</tr>
	</table>
	<br />
	<div style="width:100%;" align="center">
		<a href="user.html">
			<button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="">Back</button>
		</a>
		<a href="user.html">
			<button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="">Save</button>
		</a>
	</div>
<br />
<br />
<br />


<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['user_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User Div'); ?></dt>
		<dd>
			<?php echo h($user['User']['user_div']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Auth'); ?></dt>
		<dd>
			<?php echo h($user['User']['auth']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($user['User']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($user['User']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
	</ul>
</div>
-->