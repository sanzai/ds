<script>
$(function() {
$( ".datepicker" ).datepicker();
});

function submitStop(e){
	if (!e) var e = window.event;

		if(e.keyCode == 13)
			return false;
		}

</script>

	<br />

<?php echo $this->Form->create('User'); ?>
	<table style="float:center" width="100%">

		<br />
		<tr>
			<td colspan="8" class="t-head">ユーザ新規登録</td>
		</tr>
		<tr>
			<td width="140" class="s-head">ユーザＩＤ</td>
			<td class="data"><?php echo $this->Form->input('id', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
			<td width="140" class="s-head">ユーザ名</td>
			<td class="data"><?php echo $this->Form->input('user_name', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
		<tr>
			<td width="140" class="s-head">区分</td>
			<td class="data"><?php echo $this->Form->input('user_div', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $userOpt, 'empty'=>'-----')); ?></td>
			<td class="s-head">権限</td>
			<td class="data"><?php echo $this->Form->input('auth', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $authOpt, 'empty'=>'-----')); ?></td>
		</tr>

	</table>

	<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/users/index');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<?php echo $this->Form->submit('登　録', array('div'=>false, 'name' => 'add', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
	</div>
<?php echo $this->Form->end(); ?>

<br />
<br />
<br />




<!--
<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Add User'); ?></legend>
	<?php
		echo $this->Form->input('user_name');
		echo $this->Form->input('user_div');
		echo $this->Form->input('auth');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
	</ul>
</div>
-->