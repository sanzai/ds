<?php echo $this->Form->create('MtUnit'); ?>
<?php echo $this->Form->input('id',array('type'=>'hidden', 'value'=>$this->data['MtUnit']['id']));?>

	<table style="float:center" width="100%">

		<br />
		<tr>
			<td colspan="8" class="t-head">単位更新</td>
		</tr>
		<tr>
			<td width="140" class="s-head">単位名</td>
			<td width="340"  class="data"><?php echo $this->Form->input('item_name', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
			<td width="140" class="s-head">表示名</td>
			<td width="340"  class="data"><?php echo $this->Form->input('disp_name', array('label'=>false, 'div'=>false, 'type' => 'text', 'class'=>'if')); ?></td>
		</tr>
	</table>

	<br />
	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/MtUnits/index');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<?php echo $this->Form->submit('更　新', array('div'=>false, 'name' => 'add', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
	</div>
<?php echo $this->Form->end(); ?>

<br />
<br />
<br />



<!--
<div class="mtUnits form">
<?php echo $this->Form->create('MtUnit'); ?>
	<fieldset>
		<legend><?php echo __('Edit Mt Unit'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('item_name');
		echo $this->Form->input('disp_name');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('MtUnit.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('MtUnit.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Mt Units'), array('action' => 'index')); ?></li>
	</ul>
</div>
-->