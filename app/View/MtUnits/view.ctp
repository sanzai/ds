<div class="mtUnits view">
<h2><?php echo __('Mt Unit'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mtUnit['MtUnit']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item Name'); ?></dt>
		<dd>
			<?php echo h($mtUnit['MtUnit']['item_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Disp Name'); ?></dt>
		<dd>
			<?php echo h($mtUnit['MtUnit']['disp_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($mtUnit['MtUnit']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($mtUnit['MtUnit']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($mtUnit['MtUnit']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($mtUnit['MtUnit']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($mtUnit['MtUnit']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($mtUnit['MtUnit']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mt Unit'), array('action' => 'edit', $mtUnit['MtUnit']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mt Unit'), array('action' => 'delete', $mtUnit['MtUnit']['id']), null, __('Are you sure you want to delete # %s?', $mtUnit['MtUnit']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Mt Units'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mt Unit'), array('action' => 'add')); ?> </li>
	</ul>
</div>
