
<div class="outcomings form">
<?php echo $this->Form->create('Outcoming'); ?>
	<fieldset>
		<legend><?php echo __('Add Outcoming'); ?></legend>
	<?php
		echo $this->Form->input('incharge');
		echo $this->Form->input('out_date');
		echo $this->Form->input('created_user_id');
		echo $this->Form->input('updated_user_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('deleted_user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Outcomings'), array('action' => 'index')); ?></li>
	</ul>
</div>
