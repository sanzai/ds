<br />

	<table style="float:center" width="100%">
		<tr>
			<td colspan="7" class="t-head">出庫情報詳細</td>
		</tr>
		<tr>
			<td width="100" class="s-head">出庫商品ＩＤ</td>
			<td width="100" class="s-head">商品コード</td>
			<td width="100" class="s-head">商品名</td>
			<td width="100" class="s-head">発注数量</td>
			<td width="100" class="s-head">出庫数量</td>
			<td width="100" class="s-head">不足数量</td>
			<td width="100" class="s-head">操作</td>
		</tr>
		<?php foreach ($outcomings as $outcoming): ?>
		<tr>
			<td class="data"><?php echo h($outcoming['Outcoming']['id']); ?></td>
			<td class="data"><?php echo h($sCdOpt[$outcoming['Outcoming']['s_cd']]); ?></td>
			<td class="data"><?php echo h($outcoming['Outcoming']['item_name']); ?></td>
			<td class="data"><?php echo h($outcoming['Outcoming']['order_amount']); ?></td>
			<td class="data"><?php echo h($outcoming['Outcoming']['out_amount']); ?></td>
			<td class="data"><?php echo h($outcoming['Outcoming']['not_amount']); ?></td>
			<td class="data_center">
				<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/outcomings/edit/' . $outcoming['Outcoming']['id']);?>';">修正</button>
			</td>
		</tr>
		<?php endforeach; ?>

	<br />
	</table>

 	<br />

	<div style="width:100%;" align="center">
		<button type="button" onclick="location.href='<?php echo $this->html->url('/outcomings/index');?>';" style="width:150px;height:30px;cursor:pointer">戻　る</button>
		<?php echo $this->Form->submit('登　録', array('div'=>false, 'name' => 'add', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
	</div>
<br>




<!--
	<table style="float:center" width="100%">
		<tr>
			<td colspan="7" class="t-head">出庫詳細</td>
		</tr>
		<tr>
			<td width="100" class="s-head">出庫商品ＩＤ</td>
			<td width="100" class="s-head">商品コード</td>
			<td width="100" class="s-head">商品名</td>
			<td width="100" class="s-head">発注数量</td>
			<td width="100" class="s-head">出庫数量</td>
			<td width="100" class="s-head">不足数量</td>
			<td width="100" class="s-head">操作</td>
		</tr>
<div class="outcomings view">
<h2><?php echo __('Outcoming'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($outcoming['Outcoming']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Incharge'); ?></dt>
		<dd>
			<?php echo h($outcoming['Outcoming']['incharge']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Out Date'); ?></dt>
		<dd>
			<?php echo h($outcoming['Outcoming']['out_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($outcoming['Outcoming']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created User Id'); ?></dt>
		<dd>
			<?php echo h($outcoming['Outcoming']['created_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($outcoming['Outcoming']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated User Id'); ?></dt>
		<dd>
			<?php echo h($outcoming['Outcoming']['updated_user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($outcoming['Outcoming']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted User Id'); ?></dt>
		<dd>
			<?php echo h($outcoming['Outcoming']['deleted_user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Outcoming'), array('action' => 'edit', $outcoming['Outcoming']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Outcoming'), array('action' => 'delete', $outcoming['Outcoming']['id']), null, __('Are you sure you want to delete # %s?', $outcoming['Outcoming']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Outcomings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Outcoming'), array('action' => 'add')); ?> </li>
	</ul>
</div>
-->