<?php //var_dump($outcomings); ?>

<script type="text/javascript">

$(function() {
$( ".datepicker" ).datepicker();
});

function submitStop(e){
	if (!e) var e = window.event;

		if(e.keyCode == 13)
			return false;
		}


function toAdd(){
	document.location = "<?php echo Router::url(array('controller'=>'outcomings','action'=>'add'));?>";
}
function toClear(){
	document.location = "<?php echo Router::url(array('controller'=>'outcomings','action'=>'index'));?>";
}
function toImport(){
	document.location = "<?php echo Router::url(array('controller'=>'outcomings','action'=>'import'));?>";
}
</script>

<br />

	<?php
		echo $this->Form->create('Outcomings');
	?>
	<table style="float:center" width="100%">
		<tr>
			<td colspan="6" class="t-head-left" >検索条件</td>
		</tr>
	<tr>
			<td width="140" class="s-head">出庫日</td>
			<td width="240" class="data" colspan="3"><?php echo $this->Form->input('date_from', array('type' => 'text', 'div'=>false, 'label' => false, 'placeholder' => 'yyyy/mm/dd', 'class'=>'datepicker'));?>～<?php echo $this->Form->input('date_from', array('type' => 'text', 'div'=>false, 'label' => false, 'placeholder' => 'yyyy/mm/dd', 'class'=>'datepicker'));?></td>

	</tr>
	</table>

		<br>
	<div style="width:100%;" align="center">
		<?php echo $this->Form->submit('検　索', array('div'=>false, 'name' => 'searching', 'type'=>'submit', 'style'=>'width:150px;height:30px;cursor:pointer')); ?>
		<?php echo $this->Form->submit('クリア', array('div'=>false, 'name' => 'clear', 'type'=>'button', 'style'=>'width:150px;height:30px;cursor:pointer', 'onClick'=>'toClear()')); ?>
		<?php echo $this->Form->submit('新規読込み', array('div'=>false, 'name' => 'import', 'type'=>'button', 'style'=>'width:150px;height:30px;cursor:pointer', 'onClick'=>'toImport()')); ?>
	</div>
	<br>
		<?php echo $this->Form->end();?>

	<table style="float:center" width="100%">
		<tr>
			<td class="t-head-left">検索結果一覧</td>
			<td colspan="6" class="t-head">1/1ページ  1件目から3件目 検索結果3件 </td>
		</tr>
		<tr>
			<td width="100" class="s-head">出庫ＩＤ</td>
			<td width="200" class="s-head">出庫日</td>
			<td width="100" class="s-head">詳細</td>
		</tr>
	<?php foreach ($outcomings as $outcoming): ?>
		<tr>
			<td class="data"><?php echo h($outcoming['Outcoming']['id']); ?>&nbsp;</td>
			<td class="data"><?php echo h($outcoming['Outcoming']['out_date']); ?>&nbsp;</td>
			<td  class="data_center">
				<button style="cursor:pointer;" onclick="location.href='<?php echo $this->html->url('/outcomingDetails/index/' . $outcoming['Outcoming']['id']);?>';">詳細</button>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<br />
	<div style="width:100%;" align="center">

	</div>

<br />
<br />
<br />





<!--
	<?php foreach ($outcomings as $outcoming): ?>
	<tr>
		<td><?php echo h($outcoming['Outcoming']['id']); ?>&nbsp;</td>
		<td><?php echo h($outcoming['Outcoming']['incharge']); ?>&nbsp;</td>
		<td><?php echo h($outcoming['Outcoming']['out_date']); ?>&nbsp;</td>
		<td><?php echo h($outcoming['Outcoming']['created']); ?>&nbsp;</td>
		<td><?php echo h($outcoming['Outcoming']['created_user_id']); ?>&nbsp;</td>
		<td><?php echo h($outcoming['Outcoming']['updated']); ?>&nbsp;</td>
		<td><?php echo h($outcoming['Outcoming']['updated_user_id']); ?>&nbsp;</td>
		<td><?php echo h($outcoming['Outcoming']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($outcoming['Outcoming']['deleted_user_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $outcoming['Outcoming']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $outcoming['Outcoming']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $outcoming['Outcoming']['id']), null, __('Are you sure you want to delete # %s?', $outcoming['Outcoming']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Outcoming'), array('action' => 'add')); ?></li>
	</ul>
</div>
-->
<!--
<div id="tabmenu">
<!--
    <div id="tab_contents">
        <ul>
            <li id="tab1" name="tab1">"No1" this is tab container.you can write anythig.</li>
            <li id="tab2" name="tab2">"No2" this is tab container.you can write anythig.</li>
            <li id="tab3" name="tab3">"No3" this is tab container.you can write anythig.</li>
            <li id="tab4" name="tab4">"No4" this is tab container.you can write anythig.</li>
            <li id="tab5" name="tab5">"No5" this is tab container.you can write anythig.</li>
        </ul>
    </div>

</div>

<br />
<br />



	<table style="float:center" width="100%">
		<tr>
			<td colspan="6" class="t-head-left" >検索条件</td>
		</tr>
		<tr>
			<td width="140" class="s-head">出庫日</td>
			<td width="240" class="data" colspan="3"><input type="text" class="" >～<input type="text" class="" ></td>

	</table>
	<br />

	<div style="width:100%;" align="center">
		<button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="document.location='/vessels';">検索</button>
		<button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="document.location='/vessels/edit/12';">クリア</button>
		<a href="outcoming_import.html"><button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="document.location='/vessels';">新規読込み</button>
	</div><br>
	<table style="float:center" width="100%">
		<tr>
			<td class="t-head-left">検索結果一覧</td>
			<td colspan="6" class="t-head">1/1ページ  1件目から4件目 検索結果4件 </td>
		</tr>

	<table style="float:center" width="100%">

		<tr>
			<td width="100" class="s-head">出庫ＩＤ</td>
			<td width="200" class="s-head">出庫日</td>
			<td width="100" class="s-head">詳細</td>
		</tr>

		<tr>
			<td  class="data"><a href="vessel_detail.html">10001</a></td>
			<td  class="data">2015/09/11</td>
			<td  class="data_center"><a href="outcoming_detail.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td  class="data"><a href="vessel_detail.html">10002</a></td>
			<td  class="data">2015/09/13</td>
			<td  class="data_center"><a href="outcoming_detail.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td  class="data"><a href="vessel_detail.html">100011</a></td>
			<td  class="data">2015/09/15</td>
			<td  class="data_center"><a href="outcoming_detail.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>
		<tr>
			<td  class="data"><a href="vessel_detail.html">10012</a></td>
			<td  class="data">2015/09/16</td>
			<td  class="data_center"><a href="outcoming_detail.html"><button type="submit" style="cursor:pointer" onclick="">詳細</button></td>
		</tr>


	</table>
	<br />
<br />
<br />
<br />
-->
