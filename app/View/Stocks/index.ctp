<?php //var_dump($stocks); ?>
<br />
<script type="text/javascript">

$(function() {
$( ".datepicker" ).datepicker();
});

function submitStop(e){
	if (!e) var e = window.event;

		if(e.keyCode == 13)
			return false;
		}


function toAdd(){
	document.location = "<?php echo Router::url(array('controller'=>'stocks','action'=>'add'));?>";
}
function toClear(){
	document.location = "<?php echo Router::url(array('controller'=>'stocks','action'=>'index'));?>";
}
</script>

	<table style="float:center" width="100%">
		<tr>
			<td colspan="6" class="t-head-left" >検索条件</td>
		</tr>
		<tr>
			<td width="140" class="s-head">商品</td>
			<td width="240" class="data"><?php echo $this->Form->input('s_cd', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $sCdOpt, 'empty'=>'-----')); ?></td>
			<td width="140" class="s-head">バイヤー</td>
			<td width="240" class="data"><?php echo $this->Form->input('buyer', array('label'=>false, 'div'=>false, 'type' => 'select', 'class'=>'if', 'options' => $buyerOpt, 'empty'=>'-----')); ?></td>
		</tr>
	</table>
	<br />
	<div style="width:100%;" align="center">
		<button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="document.location='/vessels';">検索</button>
		<button type="submit" style="width:150px;height:30px;cursor:pointer" onclick="document.location='/vessels/edit/12';">クリア</button>
	</div>
	<br />

	<?php echo $this->Form->create('PackingDetail',array('action' =>'add')); ?>
	<table style="float:center" width="100%">
		<tr>
			<td class="t-head-left">検索結果一覧</td>
			<td colspan="7" class="t-head">1/1ページ  1件目から4件目 検索結果4件 </td>
		</tr>
		<tr>
			<td width="10%" class="s-head">入庫日</td>
			<td width="20%" class="s-head">商品</td>
			<td width="10%" class="s-head">産地</td>
			<td width="8%" class="s-head">規格</td>
			<td width="10%" class="s-head">在庫（CS）</td>
			<td width="10%" class="s-head">在庫（PC）</td>
			<td width="10%" class="s-head">バイヤー</td>
			<td width="22%" class="s-head">加工新規登録</td>
		</tr>
		<?php foreach ($stocks as $stock): ?>
		<tr><td  class="data"><?php echo h($stock['IncomingDetail']['incoming_date']); ?></td>
			<td  class="data"><a href="<?php echo $this->html->url('/packingDetails/viewbyscd/' . $stock['IncomingDetail']['id']);?>"><?php echo h($sCdOpt[$stock['IncomingDetail']['s_cd']]); ?></a></td>
			<td  class="data"><?php echo h($areaOpt[$stock['IncomingDetail']['area_cd']]); ?></td>
			<td  class="data_right"><?php echo h($sizeOpt[$stock['IncomingDetail']['size']]); ?></td>
			<td  class="data_right"><?php echo h($stock['IncomingDetail']['amount']); ?></td>
			<td  class="data_right"><?php echo h($stock['IncomingDetail']['amount']); ?></td>
			<td  class="data_center"><?php echo h($stock['User']['user_name']); ?></td>
			<td  class="data_center">
				<?php echo $this->Form->input('select_date'.$stock['IncomingDetail']['id'], array('type' => 'text', 'div'=>false, 'label' => false, 'placeholder' => 'yyyy/mm/dd', 'size'=>'10', 'class'=>'datepicker'));?>
				<?php echo $this->Form->submit('加工', array('div'=>false, 'name' => 'pack', 'type'=>'submit', 'style'=>'width:50px;height:25px;cursor:pointer')); ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->Form->end();?>

<br />
<br />
